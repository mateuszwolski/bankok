/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.configuration.DataInit;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aspectj.lang.annotation.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
/**
 *
 * @author worek
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@IntegrationTest
public class ExampleJUnitTest {
    @Autowired
    private IDepartamentsForLogin departamentsForLogin;
    
    @Autowired
    private DataInit di;
    
    @Test
    public void mapShouldNotBeEmpty(){
        di.truncateDB();
        di.init();
        Map<DocumentType, List<AllDepartments>> map;

//        System.out.println(departamentsForLogin.getDepartamentsForLogin("kadry1"));
//        System.out.println(departamentsForLogin.getDepartamentsForLogin("kadry2"));
//        System.out.println(departamentsForLogin.getDepartamentsForLogin("kadry3"));
        
        assertThat(departamentsForLogin.getDepartamentsForLogin("kadry1").size(), is(1));
        assertThat(departamentsForLogin.getDepartamentsForLogin("kadry2").size(), is(6));
        assertThat(departamentsForLogin.getDepartamentsForLogin("kadry3").size(), is(5));

        System.out.println();
        
        assertTrue(false);
    }
    
}
