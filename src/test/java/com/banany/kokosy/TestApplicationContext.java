/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy;

import com.banany.kokosy.configuration.SecurityConfiguration;
import com.banany.kokosy.configuration.WebMvcConfigurator;
import java.util.concurrent.TimeUnit;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 *
 * @author worek
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.banany.kokosy"})
public class TestApplicationContext{

    public static void main(String[] args) throws Exception {
       new SpringApplicationBuilder(TestApplicationContext.class)
               .showBanner(false)
               .run(args);
   }

}
