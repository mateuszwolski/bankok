package com.banany.kokosy.authentication.entity;

import com.banany.kokosy.document.entity.DocumentEditHistory;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.annotations.NaturalId;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UserAccount
        implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true,
            nullable = false)
    @NaturalId
    private String username;

    private String password;

    private boolean enabled;
    
    @OneToMany(mappedBy="personWhoChanged")
    //@JsonIgnore
    private Set<DocumentHistory> workHistory;
    
     @OneToMany(mappedBy="personWhoEdit")
    //@JsonIgnore
    private Set<DocumentEditHistory> documentEditHistory;


    protected UserAccount() {
    }

    public UserAccount(String username, String password) {
        this.username = username;
        this.password = password;
        this.enabled = true;
        this.workHistory = new HashSet<>();
        this.documentEditHistory = new HashSet<>();
    }

    public UserAccount(Long id, String username, String password,boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.id = id;
    }
    
    

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<DocumentHistory> getWorkHistory() {
        return workHistory;
    }

    public void setWorkHistory(Set<DocumentHistory> workHistory) {
        this.workHistory = workHistory;
    }

    public void addOnWorkHistory(DocumentHistory documentHistory){
        this.workHistory.add(documentHistory);
    }

    public Set<DocumentEditHistory> getDocumentEditHistory() {
        return documentEditHistory;
    }

    public void setDocumentEditHistory(Set<DocumentEditHistory> documentEditHistory) {
        this.documentEditHistory = documentEditHistory;
    }
    public void addOneWorkEditHistory(DocumentEditHistory documentEditHistory){
        this.documentEditHistory.add(documentEditHistory);
    }

    @Override
    public String toString() {
        return "UserAccount [id=" + id + ", username=" + username + "]";
    }
    
    
}
