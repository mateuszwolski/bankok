package com.banany.kokosy.authentication.entity;

import java.io.Serializable;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AuthorizationRole
        implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Long userAccountId;

    @Enumerated(EnumType.STRING)
    private RoleType name;

    public AuthorizationRole() {
    }

    public AuthorizationRole(Long userAccountId, RoleType name) {
        this.userAccountId = userAccountId;
        this.name = name;
    }

    public RoleType getName() {
        return name;
    }

    public void setRoleName(RoleType name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }
    
    

}
