package com.banany.kokosy.authentication.finder;

import java.util.Set;

import com.banany.kokosy.authentication.entity.RoleType;

public interface IAuthorizationRoleFinder {

    Set<String> getRoles(Long userAccountId);

    Set<RoleType> getRoleTypes(Long userAccountId);
}
