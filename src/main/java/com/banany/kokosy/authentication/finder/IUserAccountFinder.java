package com.banany.kokosy.authentication.finder;

import com.banany.kokosy.authentication.entity.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface IUserAccountFinder {

    UserAccount findByUsername(String username);

    UserAccount findById(Long id);
}
