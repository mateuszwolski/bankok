package com.banany.kokosy.authentication.finder.impl;

import com.banany.kokosy.authentication.repository.IUserAccountRepository;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true,
               propagation = Propagation.SUPPORTS)
@Component
public class UserAccountFinder
        implements IUserAccountFinder {

    private final IUserAccountRepository userAccountRepository;

    @Autowired
    public UserAccountFinder(IUserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Override
    public UserAccount findByUsername(String username) {
        UserAccount userAccount = userAccountRepository.findByUsername(username);

        return userAccount;
    }

    @Override
    public UserAccount findById(Long id) {
        UserAccount userAccount = userAccountRepository.findOne(id);

        return userAccount;
    }

}
