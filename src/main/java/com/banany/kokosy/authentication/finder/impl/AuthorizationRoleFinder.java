package com.banany.kokosy.authentication.finder.impl;

import com.banany.kokosy.authentication.repository.IAuthorizationRoleRepository;
import com.banany.kokosy.authentication.entity.AuthorizationRole;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.finder.IAuthorizationRoleFinder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true,
               propagation = Propagation.SUPPORTS)
@Component
public class AuthorizationRoleFinder
        implements IAuthorizationRoleFinder {

    private final IAuthorizationRoleRepository authorizationRoleRepository;

    @Autowired
    public AuthorizationRoleFinder(IAuthorizationRoleRepository authorizationRoleRepository) {
        this.authorizationRoleRepository = authorizationRoleRepository;
    }

    @Override
    public Set<String> getRoles(Long userAccountId) {
        List<AuthorizationRole> authorizationRoles = authorizationRoleRepository.findByUserAccountId(userAccountId);
        Set<String> roles = new HashSet<>();

        for (AuthorizationRole authorizationRole : authorizationRoles) {
            roles.add(authorizationRole.getName().toString());
        }

        return roles;
    }

    @Override
    public Set<RoleType> getRoleTypes(Long userAccountId) {
        List<AuthorizationRole> authorizationRoles = authorizationRoleRepository.findByUserAccountId(userAccountId);
        Set<RoleType> roles = new HashSet<>();

        for (AuthorizationRole authorizationRole : authorizationRoles) {
            roles.add(authorizationRole.getName());
        }
        return roles;
    }
    
}
