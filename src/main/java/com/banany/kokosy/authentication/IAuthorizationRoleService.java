/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.authentication;

import com.banany.kokosy.authentication.entity.AuthorizationRole;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.entity.UserAccount;

import java.util.List;

/**
 *
 * @author worek
 */
public interface IAuthorizationRoleService {
    
    List<AuthorizationRole> getAllUsersRole(Long userId);
    
    void addRole(UserAccount userAccount, RoleType roles);
    
    void addRoles(UserAccount userAccount, List<RoleType> roles);

    List<RoleType> getRoleTypesByUser(UserAccount userAccount);
    
    public List<Long> getAllUsersIds();

    List<RoleType> convertToRolTypes(List<AuthorizationRole> findAll);

//    List<UserAccount> getUsersByRole(RoleType roleType);
}
