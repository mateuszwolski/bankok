/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.authentication;

import com.banany.kokosy.authentication.repository.IAuthorizationRoleRepository;
import com.banany.kokosy.authentication.entity.AuthorizationRole;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.entity.RoleType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author worek
 */
@Service
public class AuthorizationRoleService implements IAuthorizationRoleService{

    @Autowired
    private IAuthorizationRoleRepository authorizationRoleRepository;
    
    @Override
    public List<AuthorizationRole> getAllUsersRole(Long userId) {
        return authorizationRoleRepository.findByUserAccountId(userId);
    }

    @Override
    public void addRole(UserAccount userAccount, RoleType role) {
        addRoles(userAccount, Arrays.asList(role));
    }
    
    @Override
    public void addRoles(UserAccount userAccount, List<RoleType> roles) {
        for (RoleType role : roles) {
            AuthorizationRole authorizationRole = new AuthorizationRole(userAccount.getId(), role);
            authorizationRoleRepository.save(authorizationRole);
        }
        authorizationRoleRepository.flush();
    }

    @Override
    public List<RoleType> getRoleTypesByUser(UserAccount userAccount) {
        List<AuthorizationRole> findAll = authorizationRoleRepository.findByUserAccountId(userAccount.getId());
        return convertToRolTypes(findAll);
    }
    
    @Override
    public List<Long> getAllUsersIds() {
        Set<Long> ids = new HashSet<Long>();
        List<AuthorizationRole> allUsers = authorizationRoleRepository.findAll();
        for (AuthorizationRole authorizationRole : allUsers) {
            ids.add(authorizationRole.getUserAccountId());
        }
        return new ArrayList<>(ids);
    }
    
    @Override
    public List<RoleType> convertToRolTypes(List<AuthorizationRole> findAll) {
        List<RoleType> roleTypes = new ArrayList<RoleType>();
        for (AuthorizationRole authorizationRole : findAll) {
            roleTypes.add(authorizationRole.getName());
        }
        return roleTypes;
    }
}
