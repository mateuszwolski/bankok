/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.authentication;

import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.authentication.finder.IAuthorizationRoleFinder;
import com.banany.kokosy.authentication.entity.UserAccount;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author worek
 */
@Transactional(readOnly = true,
               propagation = Propagation.SUPPORTS)
@Component("userDetailsService")
public class AuthenticationService implements UserDetailsService
{
    
 private final IUserAccountFinder userAccountFinder;

    private final IAuthorizationRoleFinder authorizationRoleFinder;

    @Autowired
    public AuthenticationService(IUserAccountFinder userAccountFinder, IAuthorizationRoleFinder authorizationRoleFinder) {
        this.userAccountFinder = userAccountFinder;
        this.authorizationRoleFinder = authorizationRoleFinder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccount userAccount = userAccountFinder.findByUsername(username);
        if (userAccount == null) {
            throw new UsernameNotFoundException("User not found");
        }

        boolean enabled = userAccount.isEnabled();
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                userAccount.getUsername(),
                userAccount.getPassword(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                grantAuthorities(userAccount));

        return userDetails;
    }

    private List<GrantedAuthority> grantAuthorities(UserAccount userAccount) {
        List<GrantedAuthority> grantedAuthoritys = new ArrayList<>();
        authorizationRoleFinder.getRoles(userAccount.getId())
                .stream()
                .forEach((String role) -> {
                    grantedAuthoritys.add(new SimpleGrantedAuthority(role));
                });

        return grantedAuthoritys;
    }
}
