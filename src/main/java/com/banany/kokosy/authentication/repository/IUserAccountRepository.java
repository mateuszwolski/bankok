package com.banany.kokosy.authentication.repository;

import com.banany.kokosy.authentication.entity.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IUserAccountRepository
        extends JpaRepository<UserAccount, Long> {

    UserAccount findByUsername(String username);

}
