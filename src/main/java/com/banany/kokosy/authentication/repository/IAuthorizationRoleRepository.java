package com.banany.kokosy.authentication.repository;

import com.banany.kokosy.authentication.entity.AuthorizationRole;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface IAuthorizationRoleRepository
        extends JpaRepository<AuthorizationRole, Long> {

    List<AuthorizationRole> findByUserAccountId(Long userAccountId);

}
