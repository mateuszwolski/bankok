/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.controller;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.service.IDocumentService;
import java.util.List;
import java.util.Set;
import javax.annotation.security.PermitAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author worek
 */
@Controller
public class DocumentController {
    @Autowired
    private IDocumentService documentService;
    
    @RequestMapping(value="/documents", method = RequestMethod.GET)
    @PermitAll
    @ResponseBody
    public List<Document> getYoursDocuments(){
        System.out.println("wwwwww");
        List<Document> list = null;
        list = documentService.getAllDocuments();
        System.out.println(list.size());
        System.out.println(list.get(0).getGenerateDate());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String name = auth.getName();
        //System.out.println(list.get(0).);
        
        Set<DocumentHistory> hh = list.get(0).getDocumentHistory();
        //hh.iterator()
                for(DocumentHistory dh : hh){
                    System.out.println(dh.getPersonWhoChanged().getUsername());
                }
        
        
        
        
        
        return list;
    }
    
    @RequestMapping(value="/document/history", method = RequestMethod.GET)
    @PermitAll
    @ResponseBody
    public void addHistory(){
        System.out.println("historie dodaje");
        List<Document> list = null;
        list = documentService.getAllDocuments();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        documentService.addHistory(list.get(0).getDocumentId(), AllDepartments.SANITARNY, auth.getName());
        System.out.println(auth.getCredentials());
        System.out.println(list.get(0).getGenerateDate());
        //Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        // String name = auth.getName();
        //System.out.println(name);
        //return list;
    }
    
}
