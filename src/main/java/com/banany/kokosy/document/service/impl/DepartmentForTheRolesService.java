/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service.impl;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;
import com.banany.kokosy.document.repository.IDepartmentForTheRolesRepository;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author worek
 */
@Service
public class DepartmentForTheRolesService implements IDepartmentForTheRolesService{

    @Autowired
    private IDepartmentForTheRolesRepository departmentForTheRolesRepository;
    
    @Override
    public List<DepartmentForTheRoles> getAllByUserRole(RoleType role) {
       return departmentForTheRolesRepository.findByRoleType(role);
    }

    @Override
    public void addDepartamentForRole(DepartmentForTheRoles depForRole) {
        departmentForTheRolesRepository.saveAndFlush(depForRole);
    }

    @Override
    public List<DepartmentForTheRoles> getAllDepartaments() {
        return departmentForTheRolesRepository.findAll();
    }

    @Override
    public List<RoleType> getRolesForDepartament(AllDepartments departament) {
        List<DepartmentForTheRoles> findAll = departmentForTheRolesRepository.findAll();
        Set<RoleType> rolesTypes = new HashSet<RoleType>();
        for (DepartmentForTheRoles departmentForTheRoles : findAll) {
            if(departmentForTheRoles.getDepartment() == departament)
                rolesTypes.add(departmentForTheRoles.getRoleType());
        }
        return  new ArrayList<RoleType>(rolesTypes);
    }
}
