/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service.impl;

import com.banany.kokosy.authentication.IAuthorizationRoleService;
import com.banany.kokosy.authentication.entity.AuthorizationRole;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentEditHistory;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.repository.IDocumentEditHistoryRepository;
import com.banany.kokosy.document.repository.IDocumentHistoryRepository;
import com.banany.kokosy.document.repository.IDocumentRepository;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 * @author worek
 */
@Service
class DocumentService implements IDocumentService {

    @Autowired
    private IDocumentRepository documentRepository;

    @Autowired
    private IUserAccountFinder accountFinder;

    @Autowired
    private IDocumentHistoryRepository documentHistoryRepository;
    
     @Autowired
    private IDocumentEditHistoryRepository documentEditHistoryRepository;

    @Autowired
    private IDocumentForTheRolesService documentForTheRolesService;

    @Autowired
    private IAuthorizationRoleService authorizationRoleService;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private IDepartmentForTheRolesService departmentForTheRolesService;

    @Override
    public List<Document> allDocumentBySource(AllDepartments source) {
        return documentRepository.findBySourceAndDataIsNotNull(source);
    }

    @Override
    public List<Document> allDocumentByDestination(AllDepartments destination) {
        return documentRepository.findByDestinationAndDataIsNotNull(destination);
    }

    @Override
    public List<Document> allDocumentByDocumentType(DocumentType documentType) {
        return documentRepository.findByDocumentTypeAndDataIsNotNull(documentType);
    }

    @Override
    public Document documentByDocumentId(UUID documentId) {
        return documentRepository.findByDocumentIdAndDataIsNotNull(documentId);
    }

    @Override
    public void addNewOrUpdateDocument(Document document) {
        documentRepository.save(document);
    }

    @Override
    public List<Document> getAllDocuments() {
        return documentRepository.findAll();
    }
    
    

    @Override
    public void addHistory(UUID documentId, AllDepartments to, String username) {
        Document document = documentRepository.findByDocumentIdAndDataIsNotNull(documentId);
        UserAccount account = accountFinder.findByUsername(username);
        DocumentHistory documentHistory = new DocumentHistory(account, document);
        documentHistory.setNewDestination(to);
        document.addHistory(documentHistory);
        document.setSource(document.getDestination());
        document.setDestination(to);
        System.out.println(document.getDocumentHistory().size());
        documentHistoryRepository.save(documentHistory);
        documentRepository.saveAndFlush(document);
    }
    
     @Override
    public void addEditHistory(UUID documentId, String comment, String username) {
        Document document = documentRepository.findByDocumentIdAndDataIsNotNull(documentId);
        UserAccount account = accountFinder.findByUsername(username);
//        DocumentHistory documentHistory = new DocumentHistory(account, document);
//        documentHistory.setNewDestination(to);
//        document.addHistory(documentHistory);
//        document.setSource(document.getDestination());
//        document.setDestination(to);
        DocumentEditHistory deh = new DocumentEditHistory(account, document);
        deh.setComment(comment);
        System.out.println(document.getDocumentEditHistory().size());
        documentEditHistoryRepository.save(deh);
        documentRepository.saveAndFlush(document);
    }

    @Override
    public List<Document> getAllUsersDocuments(String username , DocumentStatus status) {
        List<Document> documents = new ArrayList<>();

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        List<GrantedAuthority> grants = new ArrayList<>();
        for (GrantedAuthority grant : userDetails.getAuthorities()) {
            grants.add(grant);
        }

        List<DocumentType> documentType = documentForTheRolesService.findByRoleType(grants);
        for (int i = 0; i < documentType.size(); i++) {
              List<Document> docs =  documentRepository.findByDocumentTypeAndDataIsNotNull(documentType.get(i));
              readWrite(documents,docs);
        }
        List<Document> documentForDepartment = new ArrayList<>();
        
        for (GrantedAuthority grant : userDetails.getAuthorities()) {
            controleRoleAndDepartment(RoleType.valueOf(grant.getAuthority()), documents,documentForDepartment , status);
        }
        
        return documentForDepartment;
    }
    
    private void readWrite(List<Document> documents,List<Document> docs){
        for (int i =0; i<docs.size();i++) {
          documents.add(docs.get(i));
        }
    }
    
    private void controleRoleAndDepartment(RoleType role, List<Document> doc, List<Document> newDocs,DocumentStatus status){
        List<DepartmentForTheRoles> departmentForTheRoles =  departmentForTheRolesService.getAllByUserRole(role);
        for(int i = 0 ;i<departmentForTheRoles.size();i++){
            for(int j = 0 ;j<doc.size();j++){
                if(departmentForTheRoles.get(i).getDepartment().ordinal() == doc.get(j).getDestination().ordinal()){
                    if(doc.get(j).getDocumentStatus().equals(status) && !isDuplicated(newDocs,doc.get(j))){ //pobieram tylko dokumenty które sa w statusie active
                    newDocs.add(doc.get(j));
                    }
                }
            }
        }
    }

    @Override
    public void saveDocumentHistory(DocumentHistory documentHistory) {
       documentHistoryRepository.saveAndFlush(documentHistory);
    }
    
    private boolean isDuplicated(List<Document> docs, Document document){
        for (Document doc : docs) {
            if(doc.getDocumentId().toString().equals(document.getDocumentId().toString())){
                return true;
            }
        }
        return false;
    }


    @Override
    public List<Document> getAllArchiveDocuments(String username) {
        List<Document> docs = new ArrayList<>();
         UserAccount ua = accountFinder.findByUsername(username);
        for (DocumentHistory col : ua.getWorkHistory()) {
            if(col.getDocument().getDocumentStatus().equals(DocumentStatus.ARCHIVED) && notDuplicate(docs,col.getDocument())){
                docs.add(col.getDocument());
            }
        }
        
        List<Document> docs2 = getAllUsersDocuments(username,DocumentStatus.ARCHIVED);
         for (Document dd : docs2) {
            if(notDuplicate(docs,dd)){
                docs.add(dd);
            }
        }
        
        
        return docs;
    }

    private boolean notDuplicate(List<Document> docs,Document document){
        for (Document doc : docs) {
            if(doc.getDocumentId().toString().equals(document.getDocumentId().toString()))
                return false;
        }
        return true;
    }
}
