/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service.impl;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.repository.IDocumentForTheRolesRepository;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

/**
 *
 * @author worek
 * @param <E>
 */
@Service
public class DocumentForTheRolesService implements IDocumentForTheRolesService{

    @Autowired
    private IDocumentForTheRolesRepository documentForTheRolesRepository;
    
    @Override
    public List<DocumentType> findByRoleType(List<GrantedAuthority> roleType) {
        List<DocumentType> documents = new ArrayList<DocumentType>();
        for(GrantedAuthority ga : roleType){
            RoleType roleTypeFromAuth = RoleType.valueOf(ga.getAuthority());
            List<DocumentForTheRoles> documentForTheRoles = documentForTheRolesRepository.findByRoleType(roleTypeFromAuth);
            readWrite(documents,documentForTheRoles);
        }
        
        return documents;
    }
    
    private void readWrite(List<DocumentType> documents,List<DocumentForTheRoles> documentForTheRoles){
        for (int i =0; i<documentForTheRoles.size();i++) {
            if(!isDuplicated(documents,documentForTheRoles.get(i).getDocumentType()))
                documents.add(documentForTheRoles.get(i).getDocumentType());
        }
    }

    @Override
    public void addDocumentForRole(DocumentForTheRoles docForRole) {
        documentForTheRolesRepository.saveAndFlush(docForRole);
    }
    
     private boolean isDuplicated(List<DocumentType> docs, DocumentType type){
        for (DocumentType doc : docs) {
            if(doc == type){
                return true;
            }
        }
        return false;
    }

    @Override
    public List<DocumentForTheRoles> getAllDocumentTypes() {
        return documentForTheRolesRepository.findAll();
    }
    

    
}
