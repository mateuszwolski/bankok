/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author worek
 */
public interface IDocumentForTheRolesService {
    List<DocumentType> findByRoleType(List<GrantedAuthority> roleType);
    
   void addDocumentForRole(DocumentForTheRoles docForRoles);
   
   List<DocumentForTheRoles> getAllDocumentTypes();
}
