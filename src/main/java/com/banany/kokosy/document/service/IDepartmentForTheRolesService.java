/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;

import java.util.List;

/**
 *
 * @author worek
 */
public interface IDepartmentForTheRolesService {
    List<DepartmentForTheRoles> getAllByUserRole(RoleType role);
    
    void addDepartamentForRole(DepartmentForTheRoles depForRole);
    
    List<DepartmentForTheRoles> getAllDepartaments();

    List<RoleType> getRolesForDepartament(AllDepartments departament);
}
