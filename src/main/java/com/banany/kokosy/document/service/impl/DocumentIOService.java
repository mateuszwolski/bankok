package com.banany.kokosy.document.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.StreamingHttpOutputMessage.Body;
import org.springframework.stereotype.Service;

import com.banany.kokosy.api.InputAPI;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.entity.utils.DocumentTmpOb;
import com.banany.kokosy.document.repository.IDocumentRepository;
import com.banany.kokosy.document.service.IDocumentIOService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

@Service
public class DocumentIOService implements IDocumentIOService {

    private static final String FILES_LOCATION = "C:\\files\\";

    private static final String FILE_EXTENSTION = ".json";

    private static final String FILE_EXTENSTION_PDF = ".pdf";

    @Autowired
    private IDocumentRepository documentRepository;

    @Override
    public void readAll() {
        for (String filePath : readDocumentFilesList()) {
            DocumentTmpOb readFromFile = readDocObFromFile(filePath);
            Document doc = transformToDocument(readFromFile);
            documentRepository.save(doc);
        }
        documentRepository.flush();
    }

    @Override
    public void saveDocumentInFile(Document doc, String filename) {
        DocumentTmpOb docOb = transformToDocumentOb(doc);

        Gson gson = new Gson();
        String json = gson.toJson(docOb);

        saveToFile(filename, json); // Mozna przerobic by grupowało folderami np. według działów.
    }

    private List<String> readDocumentFilesList() {
//        String path = this.getClass().getResource("/").getPath().substring(1);
//        File folder = new File(path);

        File folder = new File(FILES_LOCATION);

        List<String> foundFiles = new ArrayList<String>();
        searchFiles(folder, foundFiles);

        return foundFiles;
    }

    private void searchFiles(File folder, List<String> files) {
        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                searchFiles(fileEntry, files);
            } else {
                String fileName = fileEntry.getName();
                if (fileName.endsWith(FILE_EXTENSTION)) {
                    String path = fileEntry.getAbsolutePath();
//                    String path = fileEntry.getAbsolutePath();
//                    String filePlace = path.substring(path.indexOf("test-classes") + 13);
//                    filePlace = filePlace.replace("\\", "/");
//                    files.add(filePlace);
                    files.add(path);
                    System.out.println(path);
                }
            }
        }
    }

    private static void saveToFile(String filename, String body) {
        System.out.println("ghjghjghjgj");
        System.out.println(filename);
        System.out.println(body);
        try {
            File file = new File(FILES_LOCATION + filename + FILE_EXTENSTION);
            file.createNewFile();

            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(body);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static DocumentTmpOb readDocObFromFile(String filePath) {
        try {
            Gson gson = new GsonBuilder().create();
            BufferedReader br = new BufferedReader(new FileReader(filePath));

            return gson.fromJson(br, DocumentTmpOb.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Document transformToDocument(DocumentTmpOb documentTmpOb) {
        Document doc = new Document();
        doc.setDocumentId(UUID.randomUUID());
        doc.setData(documentTmpOb.getData());

        String pattern = "dd-MM-yyyy";
        LocalDateTime localDateTime = LocalDateTime.parse(documentTmpOb.getGenerateDate(), DateTimeFormat.forPattern(pattern));

        doc.setGenerateDate(localDateTime);
        DocumentType at[] = DocumentType.values();
        AllDepartments ad[] = AllDepartments.values();

        int tmp = 0;
        for (DocumentType dd : at) {
            if (tmp == documentTmpOb.getDocumentType()) {
                doc.setDocumentType(dd);
                break;
            }
            tmp++;
        }
//      doc.setDocumentType(DocumentType.values()[documentTmpOb.getDocumentType()]); ????? // ryzyko przenumerowania
        tmp = 0;
        for (AllDepartments dd : ad) {
            if (tmp == documentTmpOb.getSource()) {
                doc.setSource(dd);
            }
            if (tmp == documentTmpOb.getDestination()) {
                doc.setDestination(dd);
            }
            tmp++;
        }
//      doc.setSource(AllDepartments.values()[documentTmpOb.getSource()]); ????? // ryzyko przenumerowania
//      doc.setDestination(AllDepartments.values()[documentTmpOb.getDestination()]); ????? // ryzyko przenumerowania
        return doc;
    }

    private DocumentTmpOb transformToDocumentOb(Document document) {
        DocumentTmpOb doc = new DocumentTmpOb();
        doc.setSource(document.getSource().ordinal());
        doc.setDestination(document.getDestination().ordinal());
        doc.setDocumentType(document.getDocumentType().ordinal());
        doc.setGenerateDate(document.getGenerateDate().toString());

        String day = document.getGenerateDate().getDayOfMonth() + "";
        String month = document.getGenerateDate().getMonthOfYear() + "";

        if (day.length() == 1) {
            day = "0" + day;
        }
        if (month.length() == 1) {
            month = "0" + month;
        }

        String generateDateAsString = day + "-" + month + "-" + document.getGenerateDate().getYear();
        doc.setGenerateDate(generateDateAsString);
        return doc;
    }

    @Override
    public void savePDF(Document document, String filename) {
        PDDocument doc = null;
        try {
            doc = new PDDocument();
            PDPage page = new PDPage();
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            PDFont pdfFont = PDType1Font.HELVETICA;
            float fontSize = 10;
            float leading = 1.5f * fontSize;

            PDRectangle mediabox = page.findMediaBox();
            float margin = 72;
            float width = mediabox.getWidth() - 2 * margin;
            float startX = mediabox.getLowerLeftX() + margin;
            float startY = mediabox.getUpperRightY() - margin;

//            String text = "I am trying to create a PDF file with a lot of text contents in the document. I am using PDFBox";
//            List<String> lines = new ArrayList<String>();
//            int lastSpace = -1;
//            while (text.length() > 0) {
//                int spaceIndex = text.indexOf(' ', lastSpace + 1);
//                if (spaceIndex < 0) {
//                    lines.add(text);
//                    text = "";
//                } else {
//                    String subString = text.substring(0, spaceIndex);
//                    float size = fontSize * pdfFont.getStringWidth(subString) / 1000;
//                    if (size > width) {
//                        if (lastSpace < 0) // So we have a word longer than the line... draw it anyways
//                        {
//                            lastSpace = spaceIndex;
//                        }
//                        subString = text.substring(0, lastSpace);
//                        lines.add(subString);
//                        text = text.substring(lastSpace).trim();
//                        lastSpace = -1;
//                    } else {
//                        lastSpace = spaceIndex;
//                    }
//                }
//            }

            contentStream.beginText();
            contentStream.setFont(pdfFont, fontSize);
            contentStream.moveTextPositionByAmount(startX, startY);
//            for (String line : lines) {
//                contentStream.drawString(line);
//                contentStream.moveTextPositionByAmount(0, -leading);
//            }
//            for (int line=0 ;line<10;line++) {
//                contentStream.drawString(line+"");
//                contentStream.moveTextPositionByAmount(0, -leading);
//            }
            
                contentStream.drawString("Numer dokumentu: "+document.getDocumentId());
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("Typ dokumentu: " + document.getDocumentType());
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("Data utworzenia " + document.getGenerateDate());
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("Dzial drukujacy dokument " + document.getDestination());
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("Data wydruku " + LocalDateTime.now());
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("");
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("");
            contentStream.moveTextPositionByAmount(0, -leading);
            contentStream.drawString("Tresc");
            contentStream.moveTextPositionByAmount(0, -leading+1);
            contentStream.drawString(document.getData() == null ? "BRAK TRESCI" : document.getData());
            contentStream.moveTextPositionByAmount(0, -leading);
            if(document.getComment() !=null && !document.getComment().isEmpty()){
            contentStream.drawString("Zmiany");
            contentStream.moveTextPositionByAmount(0, -leading+1);
            contentStream.drawString(document.getComment());
            contentStream.moveTextPositionByAmount(0, -leading);
            }
            
            contentStream.endText();
            contentStream.close();

            doc.save(FILES_LOCATION+filename+FILE_EXTENSTION_PDF);
        } catch (IOException ex) {
            Logger.getLogger(DocumentIOService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (COSVisitorException ex) {
            Logger.getLogger(DocumentIOService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (doc != null) {
                try {
                    doc.close();
                } catch (IOException ex) {
                    Logger.getLogger(DocumentIOService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
