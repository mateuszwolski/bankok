package com.banany.kokosy.document.service;

import java.util.List;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.DocumentsFlow;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;

/**
 * @author Kamil Banasiak
 */
public interface IDocumentsFlowService {

    void addFlow(DocumentsFlow flow) throws DocumentFlowAlreadyExistsException;
    
    List<DocumentsFlow> getAllDocumentFlows();

    void addNewFlow(DocumentsFlow flow) throws DocumentFlowAlreadyExistsException;
    
//    List<DocumentsFlow> getDocumentFlowsForRole(RoleType role);
}
