package com.banany.kokosy.document.service;

import java.util.List;

import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.utils.DocumentTmpOb;


public interface IDocumentIOService {
    
    void readAll();
    
    void saveDocumentInFile(Document doc, String filename);
    
    void savePDF(Document doc, String filename);
}
