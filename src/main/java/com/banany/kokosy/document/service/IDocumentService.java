/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.service;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author worek
 */
public interface IDocumentService {
    List<Document> allDocumentBySource(AllDepartments source);
    
    List<Document> allDocumentByDestination(AllDepartments destination);
    
    List<Document> allDocumentByDocumentType(DocumentType documentType);
    
    Document documentByDocumentId(UUID documentId);
    
    void addNewOrUpdateDocument(Document document);
    
     List<Document> getAllDocuments();

     void addHistory(UUID documentId, AllDepartments to, String username);
     
     List<Document> getAllUsersDocuments(String username, DocumentStatus status);
     
     void saveDocumentHistory(DocumentHistory documentHistory);
     
     List<Document> getAllArchiveDocuments(String username);
     
     void addEditHistory(UUID documentId, String comment, String username);
     
}
