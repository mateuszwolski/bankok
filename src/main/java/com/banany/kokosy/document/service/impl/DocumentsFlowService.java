package com.banany.kokosy.document.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banany.kokosy.authentication.IAuthorizationRoleService;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IAuthorizationRoleFinder;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentsFlow;
import com.banany.kokosy.document.repository.IDocumentsFlowRepository;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentsFlowService;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;

/**
 * @author Kamil Banasiak
 */
@Service
public class DocumentsFlowService implements IDocumentsFlowService {
    
    @Autowired
    private IDocumentsFlowRepository documentsFlowRepository;
    
    @Autowired
    private IDocumentForTheRolesService documentForTheRolesService;
    
    @Autowired
    private IDepartmentForTheRolesService departmentForTheRolesService;
    
    @Autowired
    private IAuthorizationRoleService authorizationRoleService;
    
    @Autowired
    private IAuthorizationRoleFinder authorizationRoleFinder;
    
    @Autowired
    private IUserAccountFinder userAccountFinder;
    
    @Override
    public void addFlow(DocumentsFlow flow) throws DocumentFlowAlreadyExistsException {
        if(documentsFlowRepository.equals(flow))
            throw new DocumentFlowAlreadyExistsException("Taki flow istnieje!");
        else {
            documentsFlowRepository.saveAndFlush(flow);
        }
    }
    
    @Override
    public void addNewFlow(DocumentsFlow flow) throws DocumentFlowAlreadyExistsException {
        addFlow(flow);
        addRolesToAllUsersForThisFlow(flow);
    }
    
    private void addRolesToAllUsersForThisFlow(DocumentsFlow flow) {
//        System.out.println("Adding new flow for all user");
        AllDepartments target = flow.getToDep();
        AllDepartments source = flow.getFromDep();
//        System.out.println("Target departament: " + target.name());
        List<RoleType> rolesForDepartament = departmentForTheRolesService.getRolesForDepartament(target);
        List<RoleType> rolesForDepartamentSource = departmentForTheRolesService.getRolesForDepartament(source);
//        System.out.println("RoleTypes " + rolesForDepartament);
        rolesForDepartament.remove(RoleType.ADMIN);
        rolesForDepartamentSource.remove(RoleType.ADMIN);
            
//        List<Long> userIds = authorizationRoleService.getAllUsersIds();
//        for (Long userId : userIds) {
//            UserAccount userById = userAccountFinder.findById(userId);
//            System.out.println("-------------");
//            System.out.println("User by ID ("+ userId + "):");
//            System.out.println(userById);
//            System.out.println("   SETTING roles " + rolesForDepartament + " to usr " + userById.getUsername());
//            authorizationRoleService.addRoles(userById, rolesForDepartament);
//        }
        for (RoleType roleType : rolesForDepartament) {
            DocumentForTheRoles docForRole = new DocumentForTheRoles();
            docForRole.setDocumentType(flow.getDocument());
            docForRole.setRoleType(roleType);
            documentForTheRolesService.addDocumentForRole(docForRole);
        }
        for (RoleType roleType : rolesForDepartamentSource) {
            DocumentForTheRoles docForRole = new DocumentForTheRoles();
            docForRole.setDocumentType(flow.getDocument());
            docForRole.setRoleType(roleType);
            documentForTheRolesService.addDocumentForRole(docForRole);
        }
    }
    
    @Override
    public List<DocumentsFlow> getAllDocumentFlows() {
        return documentsFlowRepository.findAll();
    }

//    @Override
//    public List<DocumentsFlow> getDocumentFlowsForRole(RoleType roleType) {
//        documentsFlowRepository.fi
//        return null;
//    }
    
}
