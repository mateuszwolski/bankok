package com.banany.kokosy.document.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentsFlow;


/**
 * @author Kamil Banasiak
 */
public interface IDocumentsFlowRepository extends JpaRepository<DocumentsFlow, Long> {
    
    void findByFromDep(AllDepartments fromDep);
}
