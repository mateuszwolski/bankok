/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.repository;

import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author worek
 */
public interface IDepartmentForTheRolesRepository extends JpaRepository<DepartmentForTheRoles, Long>{
    
    public List<DepartmentForTheRoles> findByRoleType(RoleType roleType);
}
