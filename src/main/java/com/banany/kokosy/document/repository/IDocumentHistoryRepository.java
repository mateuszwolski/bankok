/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.repository;

import com.banany.kokosy.document.entity.DocumentHistory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author worek
 */
public interface IDocumentHistoryRepository extends JpaRepository<DocumentHistory, Long>{
    
}
