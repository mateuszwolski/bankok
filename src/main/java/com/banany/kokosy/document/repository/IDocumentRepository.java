/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.repository;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author worek
 */
public interface IDocumentRepository extends JpaRepository<Document, Long> {
    
    List<Document> findBySourceAndDataIsNotNull(AllDepartments source);
    
    List<Document> findByDestinationAndDataIsNotNull(AllDepartments destination);
    
    List<Document> findByDocumentTypeAndDataIsNotNull(DocumentType documentType);
    
    List<Document> findByDocumentTypeAndDataIsNotNullOrderByGenerateDateDesc(DocumentType documentType);
    
    Document findByDocumentIdAndDataIsNotNull(UUID documentId);
    
    
}
//AndDataIsNotNull