/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

import com.banany.kokosy.document.entity.utils.LocalDateTimeDeserializer;
import com.banany.kokosy.document.entity.utils.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author worek
 */
@Entity
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonIgnore
    private Long id;

//    @GeneratedValue(generator = "uuid2")
//@GenericGenerator(name = "uuid2", strategy = "uuid2")
//@Column(columnDefinition = "BINARY(16)")
//@Id
    @Column(unique = true,
            nullable = false)
    @JsonIgnore
    private UUID documentId;

    @Enumerated(EnumType.ORDINAL)
    private AllDepartments source;

    @Enumerated(EnumType.ORDINAL)
    private AllDepartments destination;
    
    @Enumerated(EnumType.ORDINAL)
    private DocumentType documentType;

//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime generateDate;
    
    private String comment;

    private String data;
    @JsonIgnore
    @Enumerated(EnumType.ORDINAL)
    private DocumentStatus documentStatus;
    
     @JsonIgnore
    @OneToMany(mappedBy="documentHistory")
    private Set<DocumentHistory> documentHistory;
     
          @JsonIgnore
    @OneToMany(mappedBy="documentEditHistory")
    private Set<DocumentEditHistory> documentEditHistory;

    public Document(Long id, UUID documentId, AllDepartments source, AllDepartments destination, DocumentType documentType, LocalDateTime generateDate, String data, DocumentStatus documentStatus, Set<DocumentHistory> documentHistory
     ,Set<DocumentEditHistory> documentEditHistory) {
        this.id = id;
        this.documentId = documentId;
        this.source = source;
        this.destination = destination;
        this.documentType = documentType;
        this.generateDate = generateDate;
        this.data = data;
        this.documentStatus = documentStatus;
        this.documentHistory = documentHistory;
        this.documentHistory = documentHistory;
    }



    public Document(UUID documentId, AllDepartments source, AllDepartments destination, DocumentType documentType, LocalDateTime generateDate, String data) {
        this.documentId = documentId;
        this.source = source;
        this.destination = destination;
        this.documentType = documentType;
        this.generateDate = generateDate;
        this.data = data;
        this.documentHistory = new HashSet<>();
        this.documentStatus = DocumentStatus.ACTIVE;
    }

   
    public Document() {
        this.documentStatus = DocumentStatus.ACTIVE;
        this.generateDate = LocalDateTime.now();
        this.documentHistory = new HashSet<>();
        this.documentEditHistory = new HashSet<>();
    }

    public AllDepartments getSource() {
        return source;
    }

    public void setSource(AllDepartments source) {
        this.source = source;
    }

    public AllDepartments getDestination() {
        return destination;
    }

    public void setDestination(AllDepartments destination) {
        this.destination = destination;
    }

     @JsonSerialize(using = LocalDateTimeSerializer.class)
    public LocalDateTime getGenerateDate() {
        return generateDate;
    }
    
     @JsonDeserialize(using = LocalDateTimeDeserializer.class)   
    public void setGenerateDate(LocalDateTime generateDate) {
        this.generateDate = generateDate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
 @JsonIgnore
    public DocumentStatus getDocumentStatus() {
        return documentStatus;
    }
 @JsonIgnore
    public void setDocumentStatus(DocumentStatus documentStatus) {
        this.documentStatus = documentStatus;
    }
 @JsonIgnore
    public Set<DocumentHistory> getDocumentHistory() {
        return documentHistory;
    }
 @JsonIgnore
    public void setDocumentHistory(Set<DocumentHistory> documentHistory) {
        this.documentHistory = documentHistory;
    }
     @JsonIgnore
    public void addHistory(DocumentHistory dh){
        this.documentHistory.add(dh);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public void addEditHistory(DocumentEditHistory deh){
        this.documentEditHistory.add(deh);
    }

    public Set<DocumentEditHistory> getDocumentEditHistory() {
        return documentEditHistory;
    }

    public void setDocumentEditHistory(Set<DocumentEditHistory> documentEditHistory) {
        this.documentEditHistory = documentEditHistory;
    }
    
    
    
    
}
