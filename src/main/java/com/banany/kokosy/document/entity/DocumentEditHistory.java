/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

import com.banany.kokosy.authentication.entity.UserAccount;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.joda.time.LocalDateTime;

/**
 *
 * @author worek
 */
@Entity
public class DocumentEditHistory implements Serializable{
     @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
   
    @ManyToOne
    @JoinColumn(name="username")
    private UserAccount personWhoEdit;
    
    @ManyToOne
    @JoinColumn(name="documentId")
    private Document documentEditHistory;
    
    private LocalDateTime changedDate;
    
    private String comment;

    
    public DocumentEditHistory(UserAccount personWhoChanged, Document document) {
        this.personWhoEdit = personWhoChanged;
        this.documentEditHistory = document;
        this.changedDate = LocalDateTime.now();
    }

    public DocumentEditHistory() {
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPersonWhoEdit(UserAccount personWhoEdit) {
        this.personWhoEdit = personWhoEdit;
    }

    public void setDocumentEditHistory(Document documentEditHistory) {
        this.documentEditHistory = documentEditHistory;
    }

    public LocalDateTime getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(LocalDateTime changedDate) {
        this.changedDate = changedDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UserAccount getPersonWhoEdit() {
        return personWhoEdit;
    }

    public Document getDocumentEditHistory() {
        return documentEditHistory;
    }
    
}
