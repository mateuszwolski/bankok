/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

/**
 *
 * @author worek
 */
public enum DocumentType {

    APLIKACJA_KANDYDATA,
    KSIAZECZKA_EPIDEMIOLOGICZNA,
    ZWOLNIENIE_LEKARSKIE,
    WNIOSEK_URLOPOWY,
    MIESIECZNE_RSA,
    ZLECENIE_WYPLATY_PENSJI,
    ZAMOWIENIE_DO_WYSTAWIENIA_FAKTURY,
    FAKTURA,
    OPLACENIE_PODATKU_CELNEGO,
    ZGLOSZENIE_ZATRUDNIENIA_PRACOWNIKA,
    WYTYCZNE_MAGAZYNOWANIA_OWOCOW,
    ZAPOWIEDZ_KONTROLI_SANITARNEJ,
    RAPORT_Z_KONTROLI_SANITARNEJ,
    WYTYCZNE_TRANSPORTU_OWOCOW,
    ZAMOWIENIE,
    ZLECENIE_TRANSPORTU,
    HARMONOGRAM_DOSTAWY,
    ZLECENIE_WYDANIA_TOWARU
}
