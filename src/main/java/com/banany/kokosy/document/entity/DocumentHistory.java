/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.document.entity.utils.LocalDateTimeDeserializer;
import com.banany.kokosy.document.entity.utils.LocalDateTimeSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author worek
 */
@Entity
public class DocumentHistory implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    
    @ManyToOne
    @JoinColumn(name="username")
    private UserAccount personWhoChanged;
    
    @ManyToOne
    @JoinColumn(name="documentId")
    //@JsonIgnore
    private Document documentHistory;
    
    private LocalDateTime changedDate;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments oldSource;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments oldDestination;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments newSource;

    @Enumerated(EnumType.ORDINAL)
    private AllDepartments newDestination;
    

    public DocumentHistory(UserAccount personWhoChanged, Document document) {
        this.personWhoChanged = personWhoChanged;
        this.documentHistory = document;
        this.changedDate = LocalDateTime.now();
        this.oldSource = document.getSource();
        this.oldDestination = document.getDestination();
        this.newSource = this.oldDestination;
    }

    public DocumentHistory(Long id, UserAccount personWhoChanged, Document document, LocalDateTime changedDate, AllDepartments oldSource, AllDepartments oldDestination, AllDepartments newSource, AllDepartments newDestination) {
        this.id = id;
        this.personWhoChanged = personWhoChanged;
        this.documentHistory = document;
        this.changedDate = changedDate;
        this.oldSource = oldSource;
        this.oldDestination = oldDestination;
        this.newSource = newSource;
        this.newDestination = newDestination;
    }

    

    public DocumentHistory() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserAccount getPersonWhoChanged() {
        return personWhoChanged;
    }

    public void setPersonWhoChanged(UserAccount personWhoChanged) {
        this.personWhoChanged = personWhoChanged;
    }

    public Document getDocument() {
        return documentHistory;
    }

    public void setDocument(Document document) {
        this.documentHistory = document;
    }

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    public LocalDateTime getChangedDate() {
        return changedDate;
    }

     @JsonDeserialize(using = LocalDateTimeDeserializer.class)   
    public void setChangedDate(LocalDateTime changedDate) {
        this.changedDate = changedDate;
    }

    public AllDepartments getOldSource() {
        return oldSource;
    }

    public void setOldSource(AllDepartments oldSource) {
        this.oldSource = oldSource;
    }

    public AllDepartments getOldDestination() {
        return oldDestination;
    }

    public void setOldDestination(AllDepartments oldDestination) {
        this.oldDestination = oldDestination;
    }

    public AllDepartments getNewSource() {
        return newSource;
    }

    public void setNewSource(AllDepartments newSource) {
        this.newSource = newSource;
    }

    public AllDepartments getNewDestination() {
        return newDestination;
    }

    public void setNewDestination(AllDepartments newDestination) {
        this.newDestination = newDestination;
    }
    
    public Document getDocumentHistory() {
        return documentHistory;
    }

    public void setDocumentHistory(Document documentHistory) {
        this.documentHistory = documentHistory;
    }
    
    
    
}
