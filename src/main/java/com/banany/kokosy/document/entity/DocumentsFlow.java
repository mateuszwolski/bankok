/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Definicja flowu dokumentów w bazie
 * 
 * @author Kamil Banasiak
 */
@Entity
public class DocumentsFlow implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    private DocumentType document;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments fromDep;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments toDep;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public DocumentType getDocument() {
        return document;
    }
    
    public void setDocument(DocumentType document) {
        this.document = document;
    }

    public AllDepartments getFromDep() {
        return fromDep;
    }

    public void setFromDep(AllDepartments fromDep) {
        this.fromDep = fromDep;
    }
    
    public AllDepartments getToDep() {
        return toDep;
    }

    public void setToDep(AllDepartments toDep) {
        this.toDep = toDep;
    }

    @Override
    public boolean equals(Object obj) {
        DocumentsFlow objFlow = (DocumentsFlow) obj;
        return (this.getFromDep() == objFlow.getFromDep()
            && this.getToDep() == objFlow.getToDep()
            && this.getDocument() == objFlow.getDocument());
    }

    @Override
    public String toString() {
        return "DocumentsFlow [id=" + id + ", document=" + document + ", fromDep=" + fromDep + ", toDep=" + toDep + "]";
    }
    
    
}
