/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

import com.banany.kokosy.authentication.entity.RoleType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *Łączę tutaj department z Rolą. Mówię że np Rola Admin Występuje tylko w ZUS i Sanepid. Przykład tylko.
 * 
 * @author worek
 */
@Entity
public class DepartmentForTheRoles implements Serializable{
    
     @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @Enumerated(EnumType.ORDINAL)
    private AllDepartments department;
    
    @Enumerated(EnumType.ORDINAL)
    private RoleType roleType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AllDepartments getDepartment() {
        return department;
    }

    public void setDepartment(AllDepartments department) {
        this.department = department;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return "DepartmentForTheRoles [id=" + id + ", department=" + department + ", roleType=" + roleType + "]";
    }
    
    
}
