/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

public final class ISODateFormatingUtil {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private ISODateFormatingUtil() {
    }

    public static String format(DateTime dateTime) {
        return DATE_FORMAT.format(dateTime.toDate());
    }

    public static String format(LocalDateTime dateTime) {
        return DATE_FORMAT.format(dateTime.toDate());
    }
}