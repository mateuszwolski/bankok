/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.document.entity;

/**
 *
 * @author worek
 */
public enum AllDepartments {

    ZUS,
    PRACOWNIK,
    SEAWOW,
    CELNY,
    SANEPID,
    EKSPORTER,
    KADRY,
    SANITARNY,
    KSIEGOWO_FINANSOWY,
    IMPORT,
    MAGAZYN,
    LOGISTYKA,
    SPRZEDAZ,
    HURTOWNIA;
}
