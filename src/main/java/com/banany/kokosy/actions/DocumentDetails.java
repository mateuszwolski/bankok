/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.actions.facade.IDocumentDetailsFacade;
import com.banany.kokosy.actions.form.EditDocumentForm;
import com.banany.kokosy.actions.form.SaveInFileForm;
import com.banany.kokosy.actions.form.SendDocumentForm;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.service.IDocumentService;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author worek
 */
@Controller
public class DocumentDetails {

    @Autowired
    private IDocumentService documentService;
    @Autowired
    private IDocumentDetailsFacade documentDetailsFacade;
    @Autowired
    private IDepartamentsForLogin departamentsForLogin;

    @RequestMapping("/details")//get
    public void details(@PathParam("id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Document doc = documentService.documentByDocumentId(UUID.fromString(id));
        Map<DocumentType, List<AllDepartments>> tmp = departamentsForLogin.getDepartamentsForLogin(auth.getName());
     //   System.out.println(tmp.keySet().size());
        // System.out.println(tmp.get(doc.getDocumentType()).size());
        SendDocumentForm sdf = new SendDocumentForm();
        sdf.setPossibleDestinations(tmp.get(doc.getDocumentType()));
        model.addAttribute("sendDocumentForm", sdf);
        model.addAttribute("document", doc);
        EditDocumentForm editDocumentForm = new EditDocumentForm();
        model.addAttribute("editDocumentForm", editDocumentForm);

    }

    @RequestMapping("/send")//post
    public String detailsPost(SendDocumentForm sendDocumentForm) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        documentDetailsFacade.sendDocument(sendDocumentForm, auth.getName());
        return "redirect:/home";
    }

    @RequestMapping("/archiwizuj")
    public String archive(@PathParam("id") String id, Model model) {
        Document doc = documentService.documentByDocumentId(UUID.fromString(id));
        doc.setDocumentStatus(DocumentStatus.ARCHIVED);
        documentService.addNewOrUpdateDocument(doc);
        return "redirect:/home";

    }
    
       @RequestMapping("/zwroc")
    public String returnBackDocument(@PathParam("id") String id, Model model) {
        Document doc = documentService.documentByDocumentId(UUID.fromString(id));
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        SendDocumentForm sdf = new SendDocumentForm();
        sdf.setDocumentId(UUID.fromString(id));
        sdf.setNewDestination(doc.getSource());
        documentDetailsFacade.sendDocument(sdf, auth.getName());
        return "redirect:/home";

    }

    @RequestMapping("/write")
    public String writeDocument(SaveInFileForm saveInFileForm) {
        System.out.println("typ:" + saveInFileForm.getTypeEX());
        System.out.println("nazwa:" + saveInFileForm.getName());
        Document document = documentService.documentByDocumentId(saveInFileForm.getDocumentId());
        if (saveInFileForm.getTypeEX().equals("pdf")) {
            documentDetailsFacade.saveInPDFFileDocument(saveInFileForm.getName(), document);
        } else if (saveInFileForm.getTypeEX().equals("json")) {
            documentDetailsFacade.saveInFileDocument(saveInFileForm.getName(), document);
        }
        //documentDetailsFacade.saveInFileDocument(saveInFileForm.getName(), document);
        return "redirect:/home";

    }

    @RequestMapping("/edit")
    public String editDocument(EditDocumentForm editDocumentForm, Model mode) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Document document = documentService.documentByDocumentId(editDocumentForm.getDocumentId());
        String comment = document.getComment();
        document.setComment((comment == null ? "" : comment) + "\n" + editDocumentForm.getComment());
        documentService.addNewOrUpdateDocument(document);
        documentService.addEditHistory(editDocumentForm.getDocumentId(), editDocumentForm.getComment(), auth.getName());
        return "redirect:/details?id=" + editDocumentForm.getDocumentId().toString();
    }

}
