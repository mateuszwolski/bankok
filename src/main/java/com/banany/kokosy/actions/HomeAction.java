/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.service.IDocumentService;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author worek
 */
@Controller
public class HomeAction {

    @Autowired
    private IDocumentService documentService;

    @RequestMapping("/home")
    public void index(Locale locale, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      //  documentService.getAllUsersDocuments(auth.getName(),DocumentStatus.ACTIVE);

        List<Document> documents = documentService.getAllUsersDocuments(auth.getName(),DocumentStatus.ACTIVE);

        model.addAttribute("documents", documents);

    }

}
