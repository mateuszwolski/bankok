/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.form;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.entity.DocumentType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import org.joda.time.LocalDateTime;

/**
 *
 * @author worek
 */

public class NewDocumentForm {
    
    private UUID documentId;

    private AllDepartments source;

    private AllDepartments destination;
    
    private DocumentType documentType;

    private String data;

    public NewDocumentForm(UUID documentId) {
        this.documentId = documentId;
    }

    public NewDocumentForm() {
    }
    
    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public AllDepartments getSource() {
        return source;
    }

    public void setSource(AllDepartments source) {
        this.source = source;
    }

    public AllDepartments getDestination() {
        return destination;
    }

    public void setDestination(AllDepartments destination) {
        this.destination = destination;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    

}
