/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.form;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author worek
 */
public class SendDocumentForm {
    
    private UUID documentId;
    private AllDepartments newDestination;
    
    private List<AllDepartments> possibleDestinations;

    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

    public AllDepartments getNewDestination() {
        return newDestination;
    }

    public void setNewDestination(AllDepartments newDestination) {
        this.newDestination = newDestination;
    }

    public List<AllDepartments> getPossibleDestinations() {
        return possibleDestinations;
    }

    public void setPossibleDestinations(List<AllDepartments> possibleDestinations) {
        this.possibleDestinations = possibleDestinations;
    }
    
    
    
}
