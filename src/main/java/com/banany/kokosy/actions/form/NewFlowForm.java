package com.banany.kokosy.actions.form;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;


public class NewFlowForm {

    private DocumentType docType;

    private AllDepartments fromDep;
    
    private AllDepartments toDep;

    public DocumentType getDocType() {
        return docType;
    }

    public void setDocType(DocumentType docType) {
        this.docType = docType;
    }
    
    public AllDepartments getFromDep() {
        return fromDep;
    }

    public void setFromDep(AllDepartments fromDep) {
        this.fromDep = fromDep;
    }

    public AllDepartments getToDep() {
        return toDep;
    }
    
    public void setToDep(AllDepartments toDep) {
        this.toDep = toDep;
    }
    
}
