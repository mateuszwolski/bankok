/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.form;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.UUID;

/**
 *
 * @author worek
 */
public class SaveInFileForm {
    
    private UUID documentId;

    private String name;
    
    private String typeEX;

    public UUID getDocumentId() {
        return documentId;
    }

    public void setDocumentId(UUID documentId) {
        this.documentId = documentId;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeEX() {
        return typeEX;
    }

    public void setTypeEX(String typeEX) {
        this.typeEX = typeEX;
    }

 
    
    
    
}
