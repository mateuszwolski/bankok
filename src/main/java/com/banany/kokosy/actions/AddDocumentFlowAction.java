package com.banany.kokosy.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.banany.kokosy.actions.facade.INewDocumentFacade;
import com.banany.kokosy.actions.facade.INewFlowFacade;
import com.banany.kokosy.actions.form.NewDocumentForm;
import com.banany.kokosy.actions.form.NewFlowForm;
import com.banany.kokosy.document.service.IDocumentsFlowService;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;


/**
 * @author Kamil Banasiak
 */
@Controller
public class AddDocumentFlowAction {

    @Autowired
    private INewFlowFacade newFlowFacade;
    
    @RequestMapping(value = "/newFlow", method = RequestMethod.GET)
    public void newFlowGet(Model model) {
        NewFlowForm newFlowForm = new NewFlowForm();
         model.addAttribute("newFlowForm", newFlowForm);
        
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    }
    
    @RequestMapping(value = "/newFlow", method = RequestMethod.POST)
    public String newFlowPost(@ModelAttribute("newFlow") NewFlowForm newFlowForm) throws DocumentFlowAlreadyExistsException {
//         Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(newFlowForm.getDocType());
        System.out.println(newFlowForm.getFromDep());
        System.out.println(newFlowForm.getToDep());
         newFlowFacade.addNewFlow(newFlowForm);
        return "redirect:/home";
    }
}
