/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.actions.facade.INewDocumentFacade;
import com.banany.kokosy.actions.form.NewDocumentForm;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author worek
 */
@Controller
public class NewDocumentAction {
    
    @Autowired
    private INewDocumentFacade documentFacade;
    
     @Autowired
    private IDepartamentsForLogin departamentsForLogin;


    @RequestMapping(value = "/newDocument", method = RequestMethod.GET)
    public void newDocumentGet(Model model) {
        NewDocumentForm newDocumentForm = new NewDocumentForm(UUID.randomUUID());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AllDepartments source = documentFacade.getUsersDepartment(auth.getName());
        //List<DocumentType> types = documentFacade.getAllAllowedTypes(auth.getName());
        newDocumentForm.setSource(source);
              
        Map<DocumentType, List<AllDepartments>> tmp = departamentsForLogin.getDepartamentsForLogin(auth.getName());
        Set<DocumentType> types = tmp.keySet();
        for (DocumentType tt : tmp.keySet()) {        
             model.addAttribute(tt+"", tmp.get(tt));
        }
        model.addAttribute("map", tmp);
        model.addAttribute("newDoc", newDocumentForm);
        model.addAttribute("types", types);
        
    }
    @RequestMapping(value = "/newDocument", method = RequestMethod.POST)
    public String newDocumentPost(@ModelAttribute("newDoc") NewDocumentForm newDocumentForm) {
        System.out.println(newDocumentForm.getSource());
        System.out.println(newDocumentForm.getDestination());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        documentFacade.addNewDocument(newDocumentForm,auth.getName());
        return "redirect:/home";
    }
}