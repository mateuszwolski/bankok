/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.facade.impl;

import com.banany.kokosy.actions.facade.IDocumentDetailsFacade;
import com.banany.kokosy.actions.facade.INewDocumentFacade;
import com.banany.kokosy.actions.form.NewDocumentForm;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.repository.IDocumentForTheRolesRepository;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 * @author worek
 */
@Service
public class NewDocumentFacade implements INewDocumentFacade {

    @Autowired
    private IUserAccountFinder accountFinder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private IDocumentService documentService;
    @Autowired
    private IDepartmentForTheRolesService departmentForTheRolesService;
    @Autowired
    private IDocumentForTheRolesService documentForTheRolesService;
    @Autowired
    private IDocumentDetailsFacade documentDetailsFacade;

    @Override
    public void addNewDocument(NewDocumentForm documentForm, String username) {
    
        Document document = new Document();
       
        document.setDocumentId(documentForm.getDocumentId());
        document.setData(documentForm.getData());
        document.setDestination(documentForm.getDestination());
        document.setSource(documentForm.getSource());
        document.setDocumentType(documentForm.getDocumentType());

        documentService.addNewOrUpdateDocument(document);
        UserAccount user = accountFinder.findByUsername(username);
        DocumentHistory documentHistory = new DocumentHistory();
        documentHistory.setChangedDate(LocalDateTime.now());
        documentHistory.setDocument(document);
        documentHistory.setNewDestination(document.getDestination());
        documentHistory.setOldDestination(document.getSource());
        documentHistory.setNewSource(document.getSource());
        documentHistory.setOldSource(document.getSource());
        documentHistory.setPersonWhoChanged(user);
        document.addHistory(documentHistory);

        documentService.saveDocumentHistory(documentHistory);
        documentService.addNewOrUpdateDocument(document);
        
         if ((document.getDestination()== AllDepartments.ZUS) || (document.getDestination() == AllDepartments.CELNY)
                || (document.getDestination() == AllDepartments.EKSPORTER)
                || (document.getDestination() == AllDepartments.SANEPID)
                || (document.getDestination() == AllDepartments.SEAWOW)
                || (document.getDestination() == AllDepartments.PRACOWNIK)) {
            try {
                documentDetailsFacade.sendDocumentOutside(document);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(NewDocumentFacade.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        
    }

    @Override
    public List<DocumentType> getAllAllowedTypes(String username) {
        List<DocumentType> types = new ArrayList<>();
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        List<GrantedAuthority> grants = new ArrayList<>();
        for (GrantedAuthority grant : userDetails.getAuthorities()) {
            grants.add(grant);
        }
        types = documentForTheRolesService.findByRoleType(grants);
        return types;
    }

    @Override
    public AllDepartments getUsersDepartment(String username) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        GrantedAuthority grantt = null;
        for (GrantedAuthority grant : userDetails.getAuthorities()) {
            grantt = grant;
        }
        return departmentForTheRolesService.getAllByUserRole(RoleType.valueOf(grantt.toString())).get(0).getDepartment();
    }

}
