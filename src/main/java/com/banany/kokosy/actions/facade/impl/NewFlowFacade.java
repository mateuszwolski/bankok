package com.banany.kokosy.actions.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.banany.kokosy.actions.facade.INewFlowFacade;
import com.banany.kokosy.actions.form.NewFlowForm;
import com.banany.kokosy.document.entity.DocumentsFlow;
import com.banany.kokosy.document.service.IDocumentsFlowService;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;
import org.springframework.stereotype.Service;

@Service
public class NewFlowFacade implements INewFlowFacade {

    @Autowired
    private IDocumentsFlowService documentsFlowService;
    
    public void addNewFlow(NewFlowForm newFlowForm) throws DocumentFlowAlreadyExistsException {
        DocumentsFlow docFlow = new DocumentsFlow();
        docFlow.setDocument(newFlowForm.getDocType());
        docFlow.setFromDep(newFlowForm.getFromDep());
        docFlow.setToDep(newFlowForm.getToDep());
        
        documentsFlowService.addNewFlow(docFlow);
        
    }
}
