/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.facade;

import java.util.List;
import java.util.Map;

import com.banany.kokosy.actions.form.SendDocumentForm;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentType;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 *
 * @author worek
 */ 
public interface IDocumentDetailsFacade {
    
    void sendDocument(SendDocumentForm documentForm,String username);
    
    void sendDocumentOutside(Document document) throws JsonProcessingException;
    
    void saveInFileDocument(String name, Document document);
    
    void saveInPDFFileDocument(String name, Document document);
    
    Map<DocumentType, List<AllDepartments>> getDocFlowForLogin(String login);
}
