/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.facade.impl;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.actions.facade.IDocumentDetailsFacade;
import com.banany.kokosy.actions.form.SendDocumentForm;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.entity.utils.DocumentTmpOb;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentIOService;
import com.banany.kokosy.document.service.IDocumentService;
import com.banany.kokosy.document.service.IDocumentsFlowService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author worek
 */
@Service
public class DocumentDetailsFacade implements IDocumentDetailsFacade {

    @Autowired
    private IDocumentService documentService;

    @Autowired
    private IUserAccountFinder accountFinder;
    
    @Autowired
    private IDocumentIOService documentIOService;

    private final String URL = "http://us3r.hostingasp.pl/api/doc";

    @Autowired
    private IDepartamentsForLogin departamentsForLogin;

    @Override
    public void sendDocument(SendDocumentForm documentForm, String username) {
        Document document = documentService.documentByDocumentId(documentForm.getDocumentId());
        documentService.addHistory(document.getDocumentId(), documentForm.getNewDestination(), username);
        System.out.println("sprawdzam");
        System.out.println("destination to: "+documentForm.getNewDestination() );
        if (sendOutside(documentForm)) {
            try {
                System.out.println("if ok");
                sendDocumentOutside(document);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(DocumentDetailsFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean sendOutside(SendDocumentForm documentForm) {
        return ((documentForm.getNewDestination() == AllDepartments.ZUS) || (documentForm.getNewDestination() == AllDepartments.CELNY)
                || (documentForm.getNewDestination() == AllDepartments.EKSPORTER)
                || (documentForm.getNewDestination() == AllDepartments.SANEPID)
                || (documentForm.getNewDestination() == AllDepartments.SEAWOW)
                || (documentForm.getNewDestination() == AllDepartments.PRACOWNIK));
    }

    @Override
    public void sendDocumentOutside(Document document) throws JsonProcessingException {

        DocumentTmpOb tmp = new DocumentTmpOb();
        tmp.setData(document.getData());
        String day = document.getGenerateDate().getDayOfMonth() + "";
        String month = document.getGenerateDate().getMonthOfYear() + "";

        if (day.length() == 1) {
            day = "0" + day;
        }
        if (month.length() == 1) {
            month = "0" + month;
        }

        String generateDateAsString = day + "-" + month + "-" + document.getGenerateDate().getYear();
        tmp.setGenerateDate(generateDateAsString);
        tmp.setSource(document.getSource().ordinal());
        tmp.setDestination(document.getDestination().ordinal());
        tmp.setDocumentType(document.getDocumentType().ordinal());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(tmp);

        // Set the Content-Type header
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, requestHeaders);

// Create a new RestTemplate instance
        RestTemplate restTemplate = new RestTemplate();

// Add the Jackson and String message converters
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

// Make the HTTP POST request, marshaling the request to JSON, and the response to a String
        ResponseEntity<String> responseEntity = restTemplate.exchange(URL, HttpMethod.POST, requestEntity, String.class);
        String result = responseEntity.getBody();

    }

    @Override
    public void saveInFileDocument(String name, Document document) {
       documentIOService.saveDocumentInFile(document, name);
    }
    
    @Override
    public void saveInPDFFileDocument(String name, Document document) {
       documentIOService.savePDF(document, name);
    }

    @Override
    public Map<DocumentType, List<AllDepartments>> getDocFlowForLogin(String login) {
        return departamentsForLogin.getDepartamentsForLogin(login);
    }

}
