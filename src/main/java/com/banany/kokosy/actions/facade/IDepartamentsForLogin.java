package com.banany.kokosy.actions.facade;

import java.util.List;
import java.util.Map;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;


public interface IDepartamentsForLogin {

    Map<DocumentType, List<AllDepartments>> getDepartamentsForLogin(String login);

}
