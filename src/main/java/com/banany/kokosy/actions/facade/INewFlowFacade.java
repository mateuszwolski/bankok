
package com.banany.kokosy.actions.facade;

import com.banany.kokosy.actions.form.NewFlowForm;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;

public interface INewFlowFacade {

    void addNewFlow(NewFlowForm newFlowForm) throws DocumentFlowAlreadyExistsException;
}