/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions.facade;

import com.banany.kokosy.actions.form.NewDocumentForm;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DocumentType;
import java.util.List;

/**
 *
 * @author worek
 */
public interface INewDocumentFacade {
    
    void addNewDocument(NewDocumentForm  documentForm, String username);
    
    List<DocumentType> getAllAllowedTypes(String username);
    
    AllDepartments getUsersDepartment(String username);
    
}
