package com.banany.kokosy.actions.facade.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.authentication.IAuthorizationRoleService;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.repository.IUserAccountRepository;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.entity.DocumentsFlow;
import com.banany.kokosy.document.repository.IDocumentForTheRolesRepository;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentsFlowService;

@Service
public class DepartamentsForLogin implements IDepartamentsForLogin {

    @Autowired
    private IDocumentForTheRolesService documentForTheRolesService;
    
    @Autowired
    private IDocumentForTheRolesRepository documentForTheRolesRepository;
    
    @Autowired
    private IDepartmentForTheRolesService departmentForTheRolesService;

    @Autowired
    private IDocumentsFlowService documentsFlowService;
    
    @Autowired
    private IAuthorizationRoleService authorizationRoleService;
    
    @Autowired
    private IUserAccountRepository userAccountRepository;
    
    @Override
    public Map<DocumentType, List<AllDepartments>> getDepartamentsForLogin(String login) {
        UserAccount user = userAccountRepository.findByUsername(login);
        List<RoleType> roleTypesForUser = authorizationRoleService.getRoleTypesByUser(user);
        
        Set<AllDepartments> departamentsFromUser = new HashSet<AllDepartments>(); // Set = without duplicates
        List<DepartmentForTheRoles> allDepartamentsForTheRoles = departmentForTheRolesService.getAllDepartaments();
        
        for (DepartmentForTheRoles departmentForTheRole : allDepartamentsForTheRoles) { // 1 department --- 1+ roles
            if (roleTypesForUser.contains(departmentForTheRole.getRoleType()))  
                departamentsFromUser.add(departmentForTheRole.getDepartment());
        }
        
        Set<DocumentType> userAllowedDocs = new HashSet<DocumentType>();
        for (RoleType roleTypeForUser : roleTypesForUser) {
            List<DocumentForTheRoles> documentsForRole = documentForTheRolesRepository.findByRoleType(roleTypeForUser);
            for (DocumentForTheRoles documentForTheRoles : documentsForRole) {
                userAllowedDocs.add(documentForTheRoles.getDocumentType());
            }
        }
        List<DocumentsFlow> allDocumentFlows = documentsFlowService.getAllDocumentFlows();
        Map<DocumentType, List<AllDepartments>> map = new HashMap<DocumentType, List<AllDepartments>>();
        for (DocumentsFlow documentsFlow : allDocumentFlows) {
            AllDepartments fromDep = documentsFlow.getFromDep();
            AllDepartments toDep = documentsFlow.getToDep();
            DocumentType documentTyp = documentsFlow.getDocument();

            if (userAllowedDocs.contains(documentTyp)) {// || departamentsFromUser.contains(fromDep)) {
                if (map.containsKey(documentTyp)) {
                    List<AllDepartments> list = map.get(documentTyp);
                    if (!list.contains(toDep)) {
                        list.add(toDep);
                        map.put(documentTyp, list);
                    }
                } else {
                    map.put(documentTyp, new ArrayList<>(Arrays.asList(toDep)));
                }
            }
        }
        
        System.out.println("---------------");
        System.out.println("  MAP ");
        System.out.println(map);
        return map;
    }

}
