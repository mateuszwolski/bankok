/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.actions;

import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.authentication.finder.IUserAccountFinder;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentEditHistory;
import com.banany.kokosy.document.entity.DocumentHistory;
import com.banany.kokosy.document.entity.DocumentStatus;
import com.banany.kokosy.document.service.IDocumentService;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author worek
 */
@Controller
public class ArchiveAction {
    @Autowired
    private IDocumentService documentService;
    
    @Autowired
    private IUserAccountFinder accountFinder;

    @RequestMapping("/myArchive")
    public void index(Locale locale, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Document> documents  = documentService.getAllArchiveDocuments(auth.getName());
       
        
        //List<Document> documents = documentService.getAllUsersDocuments(auth.getName(),DocumentStatus.ARCHIVED);
        //jesli zmenimy i bedziemy brac z tej metody to dostaniemy tylko te ktore sami wyslalismy do archiwum

        model.addAttribute("documents", documents);

    }

}
