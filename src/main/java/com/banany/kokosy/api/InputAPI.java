/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.api;

import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.Document;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.entity.utils.DocumentTmpOb;
import com.banany.kokosy.document.service.IDocumentService;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import javafx.scene.input.DataFormat;
import javax.annotation.security.PermitAll;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author worek
 */
@RestController
public class InputAPI {
    
    @Autowired
    private IDocumentService documentService;

     @RequestMapping(value = "/api/input", method = RequestMethod.POST,consumes = "application/json")
     @PermitAll
    public void input(@RequestBody DocumentTmpOb input) throws IOException {
        documentService.addNewOrUpdateDocument(transformToDocument(input));   
    }
    
   // @RequestMapping(value = "/api/input", method = RequestMethod.GET)
     @PermitAll
    public void inputx(String input) throws IOException {
         System.out.println("jestem w api");
         String ss ="{\"dokument\":{\n" +
"		\"miejsceWystawienia\": \"Gdansk\",\n" +
"		\"dataSprzedazy\": \"2014-09-02\",\n" +
"		\"dataWystawienia\": \"2014-09-03\",\n" +
"		\"sprzedawca\": {\n" +
"			\"nazwa\": \"SEAwow\",\n" +
"			\"adres\": \"ul. Morska 21, 22-212 Gdansk\",\n" +
"			\"nip\": \"11111111\"\n" +
"		},\n" +
"		\"nabywca\": {\n" +
"			\"nazwa\": \"Banany i kokosy\",\n" +
"			\"adres\": \"ul. Narutowicza 21, 90-025 Lodz\",\n" +
"			\"nip\": \"11111112\"	\n" +
"		},\n" +
"		\"nrFaktury\": \"6/2011\",\n" +
"		\"pozycje\" : [\n" +
"			{ \n" +
"				\"lp\": 1, \n" +
"				\"nazwa\": \"Dowóz kontenera\", \n" +
"				\"ilosc\": 3, \n" +
"				\"jednostka\": \"szt.\",\n" +
"				\"cenaNetto\": 2000.00,\n" +
"				\"VATProcent\": 23,\n" +
"				\"wartoscNetto\": 6000.00,\n" +
"				\"VAT\": 1380.00,\n" +
"				\"wartoscBrutto\": 7380.00\n" +
"			},\n" +
"			{ \n" +
"				\"lp\": 1, \n" +
"				\"nazwa\": \"Rozladunek kontenera\", \n" +
"				\"ilosc\": 3, \n" +
"				\"jednostka\": \"szt.\",\n" +
"				\"cenaNetto\": 300.00,\n" +
"				\"VATProcent\": 23,\n" +
"				\"wartoscNetto\": 900.00,\n" +
"				\"VAT\": 207.00,\n" +
"				\"wartoscBrutto\": 1107.00\n" +
"			}\n" +
"		], \n" +
"		\"uwagi\": \"\",\n" +
"		\"wystawil\": \"Figo Fagot\"\n" +
"	},\n" +
"	\"detaleDokumentu\":{\n" +
"		\"organWystawiajacy\": \"seawow\",\n" +
"		\"typDokumentu\":\"faktura\"\n" +
"	}\n" +
"}";
         parse(ss);
    }

    public void parse(String json) throws IOException {
        JsonFactory factory = new JsonFactory();

        ObjectMapper mapper = new ObjectMapper(factory);
        JsonNode rootNode = mapper.readTree(json);

        Iterator<Map.Entry<String, JsonNode>> fieldsIterator = rootNode.fields();
        while (fieldsIterator.hasNext()) {

            Map.Entry<String, JsonNode> field = fieldsIterator.next();
            System.out.println("Key: " + field.getKey() + "\tValue:" + field.getValue());
        }
    }
    
    private Document transformToDocument(DocumentTmpOb documentTmpOb){
        Document doc = new Document();
        doc.setDocumentId(UUID.randomUUID());
        doc.setData(documentTmpOb.getData());
        
        String pattern = "dd-MM-yyyy";
        LocalDateTime localDateTime = LocalDateTime.parse(documentTmpOb.getGenerateDate(), DateTimeFormat.forPattern(pattern));
        
        doc.setGenerateDate(localDateTime);
        DocumentType at[] = DocumentType.values();
        AllDepartments ad [] = AllDepartments.values();

        int tmp = 0;
        for(DocumentType dd : at){
            if(tmp==documentTmpOb.getDocumentType()){
                doc.setDocumentType(dd);
                break;
            }
            tmp ++;
        }
        tmp=0;
        for(AllDepartments dd : ad){
            if(tmp==documentTmpOb.getSource()){
                doc.setSource(dd);
            }
            if(tmp==documentTmpOb.getDestination()){
                doc.setDestination(dd);
            }
            tmp ++;
        }
        
        
        
        
        return doc;
    }
    
}
