package com.banany.kokosy.exceptions;


public class DocumentFlowAlreadyExistsException extends Exception {

    public DocumentFlowAlreadyExistsException(String message) {
        super(message);
    }

}
