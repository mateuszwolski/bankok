/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 *
 * @author worek
 */
@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();

        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);

        return daoAuthenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .authorizeRequests().anyRequest().authenticated();
//        http
//                .formLogin().failureUrl("/login?error")
//                .defaultSuccessUrl("/")
//                .loginPage("/login")
//                .permitAll()
//                .and()
//                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
//                .permitAll();

        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/readFiles/**").permitAll()
                .antMatchers("/clean/**").permitAll()
                .antMatchers("/test/**").permitAll()
                .antMatchers("/init/**").permitAll()//allow CORS option calls
                .antMatchers("/api/**").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest().authenticated();

//        http.authorizeRequests()
//                .antMatchers("/login").permitAll();

        http.formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?error");

        http.exceptionHandling()
                .accessDeniedPage("/login?error");

        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));

//        http
//        .csrf().disable();
//        http.authorizeRequests()
//                    .antMatchers("/init").permitAll();
//        
//        http.authorizeRequests()
//                    .antMatchers("/api/*").permitAll();
//               
//        http.authorizeRequests()
//                    .antMatchers("/login").permitAll();
//
//            http.formLogin()
//                    .loginPage("/login")
//                    .defaultSuccessUrl("/home")
//                    .failureUrl("/login?error");
//
//            http.exceptionHandling()
//                    .accessDeniedPage("/login?error");
//
//            http.logout()
//                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(daoAuthenticationProvider());
        // auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
    }
}
