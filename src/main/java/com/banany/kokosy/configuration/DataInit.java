/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.configuration;

import com.banany.kokosy.actions.facade.IDepartamentsForLogin;
import com.banany.kokosy.actions.facade.impl.DepartamentsForLogin;
import com.banany.kokosy.authentication.IAuthorizationRoleService;
import com.banany.kokosy.authentication.entity.RoleType;
import com.banany.kokosy.authentication.repository.IUserAccountRepository;
import com.banany.kokosy.authentication.entity.UserAccount;
import com.banany.kokosy.document.entity.AllDepartments;
import com.banany.kokosy.document.entity.DepartmentForTheRoles;
import com.banany.kokosy.document.entity.DocumentForTheRoles;
import com.banany.kokosy.document.entity.DocumentType;
import com.banany.kokosy.document.entity.DocumentsFlow;
import com.banany.kokosy.document.service.IDepartmentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentForTheRolesService;
import com.banany.kokosy.document.service.IDocumentsFlowService;
import com.banany.kokosy.document.service.impl.DepartmentForTheRolesService;
import com.banany.kokosy.document.service.impl.DocumentForTheRolesService;
import com.banany.kokosy.document.service.impl.DocumentsFlowService;
import com.banany.kokosy.exceptions.DocumentFlowAlreadyExistsException;
import com.banany.kokosy.init.ICleanDB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pdfbox.pdfviewer.MapEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author worek
 */
@Component
public class DataInit {

    private static final String PASSWORD = "password";

    private final IUserAccountRepository userAccountRepository;

    private final IAuthorizationRoleService authorizationRoleService;
    
    private final IDocumentForTheRolesService documentForTheRolesService;
    
    private final IDepartmentForTheRolesService departmentForTheRolesService;
    
    private final IDocumentsFlowService documentsFlowService;
    
    private final IDepartamentsForLogin departamentsForLogin;
    
    private final ICleanDB cleanDB;
    
    private List<DocumentForTheRoles> documentsForRoles;
    
    private List<DepartmentForTheRoles> departmentsForTheRoles;

    private Map<String, List<RoleType>> usersAndRoles;
    
    private List<DocumentsFlow> documentsFlow;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DataInit(IUserAccountRepository userAccountRepository, PasswordEncoder passwordEncoder, IAuthorizationRoleService authorizationRoleService,
            DocumentForTheRolesService documentForTheRolesService, DepartmentForTheRolesService departmentForTheRolesService, DocumentsFlowService documentsFlowService,
            DepartamentsForLogin departamentsForLogin, ICleanDB cleanDB) {
        this.userAccountRepository = userAccountRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorizationRoleService = authorizationRoleService;
        this.documentForTheRolesService = documentForTheRolesService;
        this.departmentForTheRolesService = departmentForTheRolesService;
        this.documentsFlowService = documentsFlowService;
        this.departamentsForLogin = departamentsForLogin;
        this.cleanDB = cleanDB;
    }

    private UserAccount checkIfExist(String username) {
        UserAccount userAccount = userAccountRepository.findByUsername(username);
        return userAccount;
    }

    public void init() {
        // FIXME second init call adds entries again
        initUsers();
        initDocumentsForTheRoles();
        initDepartamentsForTheRoles();
        initDocumentsFlow();
    }

    private void initUsers() {
        initLoginsAndRoles();
        createUsersWithRoles();
    }

    private void initDocumentsForTheRoles() {
        this.documentsForRoles = new ArrayList<>();
        initDocumentsForAdminRole();
        
        addDocForRole(DocumentType.APLIKACJA_KANDYDATA, RoleType.REKRUTER);
        addDocForRole(DocumentType.KSIAZECZKA_EPIDEMIOLOGICZNA, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDocForRole(DocumentType.ZWOLNIENIE_LEKARSKIE, RoleType.KADROWY);
        addDocForRole(DocumentType.WNIOSEK_URLOPOWY, RoleType.KADROWY);
        addDocForRole(DocumentType.MIESIECZNE_RSA, RoleType.KADROWY);
        addDocForRole(DocumentType.ZLECENIE_WYPLATY_PENSJI, RoleType.KADROWY);
        addDocForRole(DocumentType.ZLECENIE_WYPLATY_PENSJI, RoleType.KSIEGOWY);
        addDocForRole(DocumentType.FAKTURA, RoleType.SPECJALISTA_DS_IMPORTU);
        addDocForRole(DocumentType.FAKTURA, RoleType.ZAOPATRZENIOWIEC);
        addDocForRole(DocumentType.FAKTURA, RoleType.KSIEGOWY);
        addDocForRole(DocumentType.FAKTURA, RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN);
        addDocForRole(DocumentType.OPLACENIE_PODATKU_CELNEGO, RoleType.SPECJALISTA_DS_CELNYCH);
        addDocForRole(DocumentType.ZGLOSZENIE_ZATRUDNIENIA_PRACOWNIKA, RoleType.KADROWY);
        addDocForRole(DocumentType.WYTYCZNE_MAGAZYNOWANIA_OWOCOW, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDocForRole(DocumentType.WYTYCZNE_MAGAZYNOWANIA_OWOCOW, RoleType.INSPEKTOR_MAGAZYNOWY);
        addDocForRole(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDocForRole(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, RoleType.INSPEKTOR_MAGAZYNOWY);
        addDocForRole(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU);
        addDocForRole(DocumentType.RAPORT_Z_KONTROLI_SANITARNEJ, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDocForRole(DocumentType.WYTYCZNE_TRANSPORTU_OWOCOW, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDocForRole(DocumentType.WYTYCZNE_TRANSPORTU_OWOCOW, RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU);
        addDocForRole(DocumentType.ZAMOWIENIE, RoleType.ZAOPATRZENIOWIEC);
        addDocForRole(DocumentType.ZAMOWIENIE, RoleType.SPRZEDAWCA);
        addDocForRole(DocumentType.ZLECENIE_TRANSPORTU, RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN);
        addDocForRole(DocumentType.ZLECENIE_TRANSPORTU, RoleType.KIEROWNIK_TRANSPORTU);
        addDocForRole(DocumentType.ZLECENIE_TRANSPORTU, RoleType.PRACOWNIK_LOGISTYCZNY);
        addDocForRole(DocumentType.ZLECENIE_TRANSPORTU, RoleType.SPECJALISTA_DS_IMPORTU);
        addDocForRole(DocumentType.HARMONOGRAM_DOSTAWY, RoleType.SPECJALISTA_DS_IMPORTU);
        addDocForRole(DocumentType.ZLECENIE_WYDANIA_TOWARU, RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN);
        addDocForRole(DocumentType.ZLECENIE_WYDANIA_TOWARU, RoleType.MAGAZYNIER);
        addDocForRole(DocumentType.ZAMOWIENIE, RoleType.SPRZEDAWCA);
        addDocForRole(DocumentType.ZAMOWIENIE, RoleType.SPECJALISTA_DS_IMPORTU);
        
        writeDocForRolesToDatabase();    
    }

    private void initDocumentsForAdminRole() {
        addDocForRole(DocumentType.APLIKACJA_KANDYDATA, RoleType.ADMIN);
        addDocForRole(DocumentType.KSIAZECZKA_EPIDEMIOLOGICZNA, RoleType.ADMIN);
        addDocForRole(DocumentType.ZWOLNIENIE_LEKARSKIE, RoleType.ADMIN);
        addDocForRole(DocumentType.WNIOSEK_URLOPOWY, RoleType.ADMIN);
        addDocForRole(DocumentType.MIESIECZNE_RSA, RoleType.ADMIN);
        addDocForRole(DocumentType.ZLECENIE_WYPLATY_PENSJI, RoleType.ADMIN);
        addDocForRole(DocumentType.FAKTURA, RoleType.ADMIN);
        addDocForRole(DocumentType.OPLACENIE_PODATKU_CELNEGO, RoleType.ADMIN);
        addDocForRole(DocumentType.ZGLOSZENIE_ZATRUDNIENIA_PRACOWNIKA, RoleType.ADMIN);
        addDocForRole(DocumentType.WYTYCZNE_MAGAZYNOWANIA_OWOCOW, RoleType.ADMIN);
        addDocForRole(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, RoleType.ADMIN);
        addDocForRole(DocumentType.RAPORT_Z_KONTROLI_SANITARNEJ, RoleType.ADMIN);
        addDocForRole(DocumentType.WYTYCZNE_TRANSPORTU_OWOCOW, RoleType.ADMIN);
        addDocForRole(DocumentType.ZLECENIE_TRANSPORTU, RoleType.ADMIN);
        addDocForRole(DocumentType.HARMONOGRAM_DOSTAWY, RoleType.ADMIN);
        addDocForRole(DocumentType.ZLECENIE_WYDANIA_TOWARU, RoleType.ADMIN);
        addDocForRole(DocumentType.ZAMOWIENIE, RoleType.ADMIN);
    }

    private void initDepartamentsForTheRoles() {
        this.departmentsForTheRoles = new ArrayList<>();
        
        initDepartamentsForAdminRole();
        addDepForRole(AllDepartments.KADRY, RoleType.REKRUTER);
        addDepForRole(AllDepartments.KADRY, RoleType.KADROWY);
        addDepForRole(AllDepartments.SANITARNY, RoleType.PRACOWNIK_DS_SANITARNYCH);
        addDepForRole(AllDepartments.IMPORT, RoleType.SPECJALISTA_DS_IMPORTU);
        addDepForRole(AllDepartments.IMPORT, RoleType.ZAOPATRZENIOWIEC);
        addDepForRole(AllDepartments.SPRZEDAZ, RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN);
        addDepForRole(AllDepartments.SPRZEDAZ, RoleType.SPRZEDAWCA);
        addDepForRole(AllDepartments.KSIEGOWO_FINANSOWY, RoleType.KSIEGOWY);
        addDepForRole(AllDepartments.KSIEGOWO_FINANSOWY, RoleType.SPECJALISTA_DS_CELNYCH);
        addDepForRole(AllDepartments.MAGAZYN, RoleType.INSPEKTOR_MAGAZYNOWY);
        addDepForRole(AllDepartments.MAGAZYN, RoleType.MAGAZYNIER);
        addDepForRole(AllDepartments.LOGISTYKA, RoleType.KIEROWNIK_TRANSPORTU);
        addDepForRole(AllDepartments.LOGISTYKA, RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU);
        addDepForRole(AllDepartments.LOGISTYKA, RoleType.PRACOWNIK_LOGISTYCZNY);
        
        writeDepForRolesToDatabase();
    }

    private void initDepartamentsForAdminRole() {
        addDepForRole(AllDepartments.KADRY, RoleType.ADMIN);
        addDepForRole(AllDepartments.SANITARNY, RoleType.ADMIN);
        addDepForRole(AllDepartments.IMPORT, RoleType.ADMIN);
        addDepForRole(AllDepartments.SPRZEDAZ, RoleType.ADMIN);
        addDepForRole(AllDepartments.KSIEGOWO_FINANSOWY, RoleType.ADMIN);
        addDepForRole(AllDepartments.MAGAZYN, RoleType.ADMIN);
        addDepForRole(AllDepartments.LOGISTYKA, RoleType.ADMIN);
        
        addDepForRole(AllDepartments.PRACOWNIK, RoleType.ADMIN);
        addDepForRole(AllDepartments.ZUS, RoleType.ADMIN);
        addDepForRole(AllDepartments.EKSPORTER, RoleType.ADMIN);
        addDepForRole(AllDepartments.SEAWOW, RoleType.ADMIN);
        addDepForRole(AllDepartments.CELNY, RoleType.ADMIN);
        addDepForRole(AllDepartments.SANEPID, RoleType.ADMIN);
        addDepForRole(AllDepartments.HURTOWNIA, RoleType.ADMIN);
    }

    private void writeDocForRolesToDatabase() {
        documentsForRoles.stream().forEach((docForRole) -> {
               documentForTheRolesService.addDocumentForRole(docForRole);
               
        });    
    }
    
    private void writeDepForRolesToDatabase() {
        departmentsForTheRoles.stream().forEach((departmentForTheRoles) -> {
           departmentForTheRolesService.addDepartamentForRole(departmentForTheRoles);
        });
    }
    
    private void addDocForRole(DocumentType doc, RoleType role) {
        DocumentForTheRoles docForRole = new DocumentForTheRoles();
        docForRole.setDocumentType(doc);
        docForRole.setRoleType(role);
        documentsForRoles.add(docForRole);
    }
    
    private void addDepForRole(AllDepartments dep, RoleType role) {
        DepartmentForTheRoles depForRole = new DepartmentForTheRoles();
        depForRole.setDepartment(dep);
        depForRole.setRoleType(role);
        departmentsForTheRoles.add(depForRole);
    }

    private void initLoginsAndRoles() {
        usersAndRoles = new HashMap<String, List<RoleType>>();
        
        usersAndRoles.put("admin", roles(RoleType.ADMIN));
        
        usersAndRoles.put("kadry1", roles(RoleType.REKRUTER));
        usersAndRoles.put("kadry2", roles(RoleType.REKRUTER, RoleType.KADROWY));
        usersAndRoles.put("kadry3", roles(RoleType.KADROWY));
        
        usersAndRoles.put("sanitarny1", roles(RoleType.PRACOWNIK_DS_SANITARNYCH));
        
        usersAndRoles.put("import1", roles(RoleType.SPECJALISTA_DS_IMPORTU, RoleType.ZAOPATRZENIOWIEC));
        usersAndRoles.put("import2", roles(RoleType.SPECJALISTA_DS_IMPORTU));
        usersAndRoles.put("import3", roles(RoleType.ZAOPATRZENIOWIEC));
        
        usersAndRoles.put("sprzedaz1", roles(RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN, RoleType.SPRZEDAWCA));
        usersAndRoles.put("sprzedaz2", roles(RoleType.SPECJALISTA_DS_REALIACJI_ZAMOWIEN));
        usersAndRoles.put("sprzedaz3", roles(RoleType.SPRZEDAWCA));
        
        usersAndRoles.put("ksiegowy1", roles(RoleType.KSIEGOWY, RoleType.SPECJALISTA_DS_CELNYCH));
        usersAndRoles.put("ksiegowy2", roles(RoleType.KSIEGOWY));
        usersAndRoles.put("ksiegowy3", roles(RoleType.SPECJALISTA_DS_CELNYCH));

        usersAndRoles.put("magazyn1", roles(RoleType.INSPEKTOR_MAGAZYNOWY, RoleType.MAGAZYNIER));
        usersAndRoles.put("magazyn2", roles(RoleType.INSPEKTOR_MAGAZYNOWY));
        usersAndRoles.put("magazyn3", roles(RoleType.MAGAZYNIER));
        
        usersAndRoles.put("logistyka1", roles(RoleType.KIEROWNIK_TRANSPORTU, RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU, RoleType.PRACOWNIK_LOGISTYCZNY));
        usersAndRoles.put("logistyka2", roles(RoleType.KIEROWNIK_TRANSPORTU));
        usersAndRoles.put("logistyka3", roles(RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU));
        usersAndRoles.put("logistyka4", roles(RoleType.PRACOWNIK_LOGISTYCZNY));
        usersAndRoles.put("logistyka5", roles(RoleType.KIEROWNIK_TRANSPORTU, RoleType.PRACOWNIK_LOGISTYCZNY));
        usersAndRoles.put("logistyka6", roles(RoleType.KIEROWNIK_TRANSPORTU, RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU));
        usersAndRoles.put("logistyka7", roles(RoleType.INSPEKTOR_DS_WARUNKOW_TRANSPORTU, RoleType.PRACOWNIK_LOGISTYCZNY));
    }

    private List<RoleType> roles(RoleType... roles) {
        return new ArrayList<RoleType>(Arrays.asList(roles));
    }

    private void createUsersWithRoles() {
        for(Entry<String, List<RoleType>> entry : usersAndRoles.entrySet()) {
            String login = entry.getKey();
            List<RoleType> roles = entry.getValue();
            
            createUserWithRoles(login, roles);
        }
    }
    
    private void createUserWithRoles(String login, List<RoleType> roles) {
        UserAccount userEx = checkIfExist(login);
        if (userEx == null) {
            userEx = createAndReturnUser(login);
            authorizationRoleService.addRoles(userEx, roles);
        }
    }

    private UserAccount createAndReturnUser(String login) {
        UserAccount userAccount = new UserAccount(login, passwordEncoder.encode(PASSWORD));
        userAccountRepository.saveAndFlush(userAccount);
        
        // return newly created user from system by login
        return checkIfExist(login);
    }
    
    private void initDocumentsFlow() {
        documentsFlow = new ArrayList<>();
        // 1
        addDocFlow(DocumentType.APLIKACJA_KANDYDATA, AllDepartments.PRACOWNIK, AllDepartments.KADRY);
        // 2
        addDocFlow(DocumentType.KSIAZECZKA_EPIDEMIOLOGICZNA, AllDepartments.PRACOWNIK, AllDepartments.KADRY);
        addDocFlow(DocumentType.KSIAZECZKA_EPIDEMIOLOGICZNA, AllDepartments.KADRY, AllDepartments.SANITARNY);
        // 3
        addDocFlow(DocumentType.ZWOLNIENIE_LEKARSKIE, AllDepartments.PRACOWNIK, AllDepartments.KADRY);
        // 4
        addDocFlow(DocumentType.WNIOSEK_URLOPOWY, AllDepartments.PRACOWNIK, AllDepartments.KADRY);
        // 5
        addDocFlow(DocumentType.MIESIECZNE_RSA, AllDepartments.KADRY, AllDepartments.ZUS);
        // 6
        addDocFlow(DocumentType.ZLECENIE_WYPLATY_PENSJI, AllDepartments.KADRY, AllDepartments.KSIEGOWO_FINANSOWY);
        // 8
        addDocFlow(DocumentType.FAKTURA, AllDepartments.IMPORT, AllDepartments.KSIEGOWO_FINANSOWY);
        addDocFlow(DocumentType.FAKTURA, AllDepartments.SPRZEDAZ, AllDepartments.KSIEGOWO_FINANSOWY);
        addDocFlow(DocumentType.FAKTURA, AllDepartments.SPRZEDAZ, AllDepartments.HURTOWNIA);
        addDocFlow(DocumentType.FAKTURA, AllDepartments.EKSPORTER, AllDepartments.IMPORT);
        addDocFlow(DocumentType.FAKTURA, AllDepartments.SEAWOW, AllDepartments.IMPORT);
        // 9
        addDocFlow(DocumentType.OPLACENIE_PODATKU_CELNEGO, AllDepartments.CELNY, AllDepartments.KSIEGOWO_FINANSOWY);
        // 10
        addDocFlow(DocumentType.ZGLOSZENIE_ZATRUDNIENIA_PRACOWNIKA, AllDepartments.KADRY, AllDepartments.ZUS);
        // 11
        addDocFlow(DocumentType.WYTYCZNE_MAGAZYNOWANIA_OWOCOW, AllDepartments.SANITARNY, AllDepartments.MAGAZYN);
        addDocFlow(DocumentType.WYTYCZNE_MAGAZYNOWANIA_OWOCOW, AllDepartments.SANEPID, AllDepartments.SANITARNY);
        // 12
        addDocFlow(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, AllDepartments.SANITARNY, AllDepartments.MAGAZYN);
        addDocFlow(DocumentType.ZAPOWIEDZ_KONTROLI_SANITARNEJ, AllDepartments.SANITARNY, AllDepartments.LOGISTYKA);
        // 13
        addDocFlow(DocumentType.RAPORT_Z_KONTROLI_SANITARNEJ, AllDepartments.SANITARNY, AllDepartments.SANITARNY);
        /// 14
        addDocFlow(DocumentType.WYTYCZNE_TRANSPORTU_OWOCOW, AllDepartments.SANEPID, AllDepartments.SANITARNY);
        addDocFlow(DocumentType.WYTYCZNE_TRANSPORTU_OWOCOW, AllDepartments.SANITARNY, AllDepartments.LOGISTYKA);
        // 15
        addDocFlow(DocumentType.ZAMOWIENIE, AllDepartments.HURTOWNIA, AllDepartments.SPRZEDAZ);
        addDocFlow(DocumentType.ZAMOWIENIE, AllDepartments.IMPORT, AllDepartments.EKSPORTER);
        // 16
        addDocFlow(DocumentType.ZLECENIE_TRANSPORTU, AllDepartments.SPRZEDAZ, AllDepartments.LOGISTYKA);
        addDocFlow(DocumentType.ZLECENIE_TRANSPORTU, AllDepartments.IMPORT, AllDepartments.LOGISTYKA);
        addDocFlow(DocumentType.ZLECENIE_TRANSPORTU, AllDepartments.IMPORT, AllDepartments.SEAWOW);
        // 17
        addDocFlow(DocumentType.HARMONOGRAM_DOSTAWY, AllDepartments.IMPORT, AllDepartments.SEAWOW);
        // 18
        addDocFlow(DocumentType.ZLECENIE_WYDANIA_TOWARU, AllDepartments.SPRZEDAZ, AllDepartments.MAGAZYN);
        
        writeFlowToDatabase();
    }

    private void addDocFlow(DocumentType docTyp, AllDepartments from, AllDepartments to) {
        DocumentsFlow docFlow = new DocumentsFlow();
        docFlow.setDocument(docTyp);
        docFlow.setFromDep(from);
        docFlow.setToDep(to);
        documentsFlow.add(docFlow);
    }

    private void writeFlowToDatabase() {
        for (DocumentsFlow flow : documentsFlow) {
            try {
                documentsFlowService.addFlow(flow);
            } catch (DocumentFlowAlreadyExistsException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void test() {
        UserAccount user = checkIfExist("kadry2");
        List<RoleType> roleTypesByUser = authorizationRoleService.getRoleTypesByUser(user);
        System.out.println("User kadry2 roles: " + roleTypesByUser);
    }
    
    public void truncateDB() {
        System.out.println("+++++++++++ TRUNCATE +++++++++++");
        cleanDB.cleanDB();
    }
}
