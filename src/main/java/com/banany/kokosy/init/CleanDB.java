package com.banany.kokosy.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banany.kokosy.authentication.repository.IAuthorizationRoleRepository;
import com.banany.kokosy.authentication.repository.IUserAccountRepository;
import com.banany.kokosy.document.repository.IDepartmentForTheRolesRepository;
import com.banany.kokosy.document.repository.IDocumentEditHistoryRepository;
import com.banany.kokosy.document.repository.IDocumentForTheRolesRepository;
import com.banany.kokosy.document.repository.IDocumentHistoryRepository;
import com.banany.kokosy.document.repository.IDocumentRepository;
import com.banany.kokosy.document.repository.IDocumentsFlowRepository;

@Service
public class CleanDB implements ICleanDB {

    @Autowired
    private IDepartmentForTheRolesRepository departmentForTheRolesRepository;

    @Autowired
    private IAuthorizationRoleRepository authorizationRoleRepository;

    @Autowired
    private IDocumentEditHistoryRepository documentEditHistoryRepository;
    
    @Autowired
    private IUserAccountRepository userAccountRepository;
    
    @Autowired
    private IDocumentForTheRolesRepository documentForTheRolesRepository;
    
    @Autowired
    private IDocumentHistoryRepository documentHistoryRepository;

    @Autowired
    private IDocumentRepository documentRepository;
    
    @Autowired
    private IDocumentsFlowRepository documentsFlowRepository;

    @Override
    public void cleanDB() {
        documentHistoryRepository.deleteAll();
        documentsFlowRepository.deleteAll();
        departmentForTheRolesRepository.deleteAll();
        documentEditHistoryRepository.deleteAll();
        documentForTheRolesRepository.deleteAll();
        documentRepository.deleteAll();
        authorizationRoleRepository.deleteAll();
        userAccountRepository.deleteAll();
    }
    
}
