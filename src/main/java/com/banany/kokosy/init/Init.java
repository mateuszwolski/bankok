/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.banany.kokosy.init;

//import org.springframework.web.bind.annotation.RequestMapping;
import com.banany.kokosy.configuration.DataInit;
import com.banany.kokosy.document.service.IDocumentIOService;
import java.util.Locale;
import javax.annotation.security.PermitAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author worek
 */
@Controller
public class Init {

    @Autowired
    private DataInit di;
    
    @Autowired
    private IDocumentIOService documentIOService;

    @RequestMapping("/init")
    @PermitAll
    public void init(Locale locale, Model model) {
        di.init();
        //return "/";
    }
    
    @RequestMapping("/readFiles")
    @PermitAll
    public void readFiles() {
        System.out.println("wczytuje");
        documentIOService.readAll();
        System.out.println("przeszło");
    }

    @RequestMapping("/test")
    @PermitAll
    public void test(Locale locale, Model model) {
        di.test();
        //return "/";
    }
    
    @RequestMapping("/clean")
    @PermitAll
    public void clean(Locale locale, Model model) {
        di.truncateDB();
        //return "/";
    }

}
